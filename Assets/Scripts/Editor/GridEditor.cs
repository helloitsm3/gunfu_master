﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (Grid))]
public class GridEditor : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Grid grid = (Grid)target;
        grid.GenerateMap();
    }
}
