﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AudioSingleton : MonoBehaviour 
{
    public static AudioSingleton instance;
    List<AudioSource> soundList;

    public enum SOUNDS
    {
        SOUND_MAINMENU,
        SOUND_BUTTONCLICK,
        SOUND_GAMEPLAY,
        SOUND_PISTOL,
        SOUND_RIFLE,
        SOUND_CRATE,
        SOUND_GRENADE,
        SOUND_EXPLOSION,
        SOUND_MELEE,
        SOUND_KNOCKBACK,
        SOUND_FALLING,
        SOUND_SNOWBALL,
        SOUND_TIMEBOMB,
        SOUND_GOAL,
        SOUND_OUT,
        SOUND_ULTIMATE,
    };

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        soundList = new List<AudioSource>();
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            soundList.Add(gameObject.transform.GetChild(i).GetComponent<AudioSource>());
        }

        // PLAY BACKGROUND SOUND
        PlaySound(SOUNDS.SOUND_MAINMENU, 0.025f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PlaySound(SOUNDS sound, float volume)
    {
        soundList[(int)sound].volume = volume;
        soundList[(int)sound].Play();
    }

    public void stopSound(SOUNDS sound)
    {
        soundList[(int)sound].Stop();
    }

    public void PlayButtonClick()
    {
        soundList[(int)SOUNDS.SOUND_BUTTONCLICK].volume = 0.1f;
        soundList[(int)SOUNDS.SOUND_BUTTONCLICK].Play();
    }
}
