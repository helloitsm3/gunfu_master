﻿using UnityEngine;
using System.Collections;

public class CustomMapManager : MonoBehaviour 
{
    public GameObject SnowMan;
    private Transform tilesHolder;

    // Use this for initialization
	void Start () 
    {
        tilesHolder = new GameObject("Tiles").transform;
        
        if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
        {
            SaveLoadManager.OpenFile(PlayerPrefs.GetString("Loading_Map_Name"));
        }
        
        else
        {
            SaveLoadManager.OpenFile(PlayerPrefs.GetString("GAMEMODE"));
        }
       
        LoadMapByName();
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void LoadMapByName()
    {
        for (int i = 0; i < SaveLoadManager.LoadMapData()[0].totalTileSize; i++)
        {
            float posX = SaveLoadManager.LoadMapData()[i].tilePosX;
            float posY = SaveLoadManager.LoadMapData()[i].tilePosY;
            float sizeX = SaveLoadManager.LoadMapData()[i].tileSizeX;
            float sizeY = SaveLoadManager.LoadMapData()[i].tileSizeY;
            float mapSizeX = SaveLoadManager.LoadMapData()[i].mapSizeX;
            float mapSizeY = SaveLoadManager.LoadMapData()[i].mapSizeY;

            string tileTag = SaveLoadManager.LoadMapData()[i].tileTag;
            string tileName = SaveLoadManager.LoadMapData()[i].tileName;
            string tileLayer = SaveLoadManager.LoadMapData()[i].tileLayer;

            PlayerPrefs.SetInt("MAP_WIDTH_SIZE", (int)mapSizeX);
            PlayerPrefs.SetInt("MAP_HEIGHT_SIZE", (int)mapSizeY);

            GameObject temp = Resources.Load<GameObject>("Prefabs/Custom_Map_Editor/" + tileTag);

            if (tileTag == "Interactable" && tileName == "30")
            {
                GameObject tempGO = Instantiate(SnowMan);
                tempGO.transform.position = new Vector3(posX, posY + 0.45f, -0.9f);
                tempGO.transform.localScale = new Vector2(1.5f, 1.5f);
                tempGO.transform.SetParent(tilesHolder);
            }
            else
            {
                GameObject tempGO = Instantiate(temp.transform.GetChild(int.Parse(tileName) - 1).gameObject);
                tempGO.transform.position = new Vector3(posX, posY, -0.9f);
                tempGO.transform.localScale = new Vector2(sizeX, sizeY);
                tempGO.transform.SetParent(tilesHolder);

                if (tempGO.GetComponent<Rigidbody2D>() != null)
                {
                    tempGO.GetComponent<Rigidbody2D>().isKinematic = true;
                }
            }
        }
    }
}
