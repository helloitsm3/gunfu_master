﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomMapButton : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void TaskOnClick()
    {
        string buttonName = EventSystem.current.currentSelectedGameObject.transform.GetChild(0).GetComponent<Text>().text;
        PlayerPrefs.SetString("Loading_Map_Name", buttonName);
        UIManagerScript.instance.customMapSelected = true;
    }
}
