﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Linq;
using System;

public class CustomMapEditor : MonoBehaviour
{
    public static CustomMapEditor instance;
    public InputField inputfield;
    public GameObject MapInitials;
    public GameObject LoadMapByName;
    public GameObject gridObj;
    public GameObject[] MapsGO;

    [Header("Sprites Preview")]
    public GameObject desertSprites;
    public GameObject forestSprites;
    public GameObject winterSprites;
    public GameObject scifiSprites;
    public GameObject interactablesSprites;
    public GameObject otherSprites;

    [HideInInspector]
    public List<Vector3> tilePosList = new List<Vector3>();
    [HideInInspector]
    public GameObject tileTemp;
    [HideInInspector]
    public int mapX = 1, mapY = 1;
    [HideInInspector]
    public List<TilesData> tileDataList = new List<TilesData>();
    [HideInInspector]
    public string mapName;
    [HideInInspector]
    public bool isMapUpdate = false;
    [HideInInspector]
    public bool isMapPrompt = true;

    [Header("Map Extension Inputs")]
    public InputField WidthInput;
    public InputField HeightInput;

    public bool spawnElement = false;
    private bool isMapExUpdate = false;
    private float rayRange = 250.0f;
    private Transform tilesHolder;
    //private Vector3 tileVec3 = new Vector3(); COMMENTED AS VARIABLE IS CURRENTLY UNUSED
    private string s_spriteName;
    private int mapSizeX = 2;
    private int mapSizeY = 2;
    private int LoadCounter = 0;
    private string mapLoadName = "";

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        MapInitials.SetActive(true);
        LoadMapByName.SetActive(false);
        tilesHolder = new GameObject("Tiles").transform;
        WidthInput.text = "2";
        HeightInput.text = "2";

        mapSizeX = PlayerPrefs.GetInt("MAP_WIDTH_SIZE");
        mapSizeY = PlayerPrefs.GetInt("MAP_HEIGHT_SIZE");

        foreach (GameObject GO in MapsGO)
        {
            GO.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        SpawnTilesOnGrid();
        RemoveTilesOnGrid();
        ExpandMapWdthHeight();
        UpdateMainCamera();
        UpdateCustomMap();
    }

    void UpdateCustomMap()
    {
        if (!isMapPrompt)
        {
            MapInitials.SetActive(true);
            isMapUpdate = false;
            isMapPrompt = true;
            inputfield.text = "";
            WidthInput.text = "2";
            HeightInput.text = "2";

            foreach (GameObject GO in MapsGO)
            {
                GO.gameObject.SetActive(false);
            }
        }
    }

    void ExpandMapWdthHeight()
    {
        int maxHeight = 50;
        int maxWidth = 50;

        if (isMapExUpdate)
        {
            if (int.Parse(WidthInput.text) <= maxWidth)
            {
                if (int.Parse(HeightInput.text) <= maxHeight)
                {
                    gridObj.GetComponent<Grid>().GenerateMap(int.Parse(WidthInput.text), int.Parse(HeightInput.text));
                    mapSizeX = int.Parse(WidthInput.text);
                    mapSizeY = int.Parse(HeightInput.text);
                    isMapExUpdate = false;
                }
            }

            if (int.Parse(WidthInput.text) > maxWidth)
            {
                WidthInput.text = maxWidth.ToString();

                gridObj.GetComponent<Grid>().GenerateMap(int.Parse(WidthInput.text), int.Parse(HeightInput.text));
                mapSizeX = int.Parse(WidthInput.text);
                mapSizeY = int.Parse(HeightInput.text);
                isMapExUpdate = false;
            }

            if (int.Parse(HeightInput.text) > maxHeight)
            {
                HeightInput.text = maxHeight.ToString();

                gridObj.GetComponent<Grid>().GenerateMap(int.Parse(WidthInput.text), int.Parse(HeightInput.text));
                mapSizeX = int.Parse(WidthInput.text);
                mapSizeY = int.Parse(HeightInput.text);
                isMapExUpdate = false;
            }
        }
    }

    void SpawnTilesOnGrid()
    {
        if (Input.GetMouseButton(0) && spawnElement)        // SPAWNING TILES WHILE HOLDING DOWN LEFT MOUSE BUTTON
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 100.0f);
            RaycastHit hit;

            if (hit2D.collider != null)
            {
                Destroy(hit2D.collider.gameObject);
            }

            if (Physics.Raycast(ray, out hit, rayRange))
            {
                GameObject gridGO = gridObj;

                GameObject tileGO = Instantiate(AssignObjects(TilesetHandler.instance.b_Name).transform.GetChild(int.Parse(s_spriteName) - 1).gameObject);
                tileGO.transform.position = new Vector3(hit.transform.position.x, hit.transform.position.y, hit.transform.position.z - 0.1f);
                tileGO.transform.localScale = Vector3.one * (1 - gridGO.GetComponent<Grid>().outlinePercent - 0.2f);
                tileGO.transform.SetParent(tilesHolder);
                tileGO.name = s_spriteName;

                CheckObjectOverlap(tileGO);

                if (tileGO.GetComponent<Rigidbody2D>() != null)
                {
                    tileGO.GetComponent<Rigidbody2D>().isKinematic = true;
                }
            }
        }
    }

    void CheckObjectOverlap(GameObject temp_GO)
    {
        if (temp_GO.GetComponent<BoxCollider2D>())
        {
            BoxCollider2D tempBC = temp_GO.GetComponent<BoxCollider2D>();
            Collider2D[] overlap = Physics2D.OverlapAreaAll(tempBC.bounds.min + new Vector3(0.5f, 0.5f), tempBC.bounds.max - new Vector3(0.5f, 0.5f));

            if (overlap.Length > 1)
            {
                Destroy(temp_GO);
                Debug.Log(overlap.Length);
            }
        }
        else
        {
            temp_GO.AddComponent<BoxCollider2D>();
        }
    }

    void GetTilesData()
    {
        Transform tileSet = GameObject.Find("Tiles").transform;
        mapSizeX = PlayerPrefs.GetInt("MAP_WIDTH_SIZE");
        mapSizeY = PlayerPrefs.GetInt("MAP_HEIGHT_SIZE");
        TilesData tilesData;

        if (tileDataList != null)
        {
            tileDataList.Clear();
            SaveLoadManager.LoadMapData().Clear();
        }

        for (int i = 0; i < tileSet.transform.childCount; i++)
        {
            int tilenameCut;
            bool isNameint = int.TryParse(tileSet.GetChild(i).name, out tilenameCut);

            if (isNameint)
            {
                if (tileSet.transform.GetChild(i).name == "34" && tileSet.transform.GetChild(i).tag == "Interactable")
                {
                    Vector3 wormPos = WormTiles.instance.initialPos;

                    tilesData = new TilesData(int.Parse(tileSet.GetChild(i).name), tileSet.transform.childCount, LayerMask.LayerToName(tileSet.GetChild(i).gameObject.layer), tileSet.GetChild(i).tag,
                    tileSet.GetChild(i).name, wormPos.x, wormPos.y,
                    tileSet.GetChild(i).gameObject.transform.localScale.x, tileSet.GetChild(i).gameObject.transform.localScale.y, mapSizeX, mapSizeY);
                }
                else if (tileSet.transform.GetChild(i).name == "21" && tileSet.transform.GetChild(i).tag == "Interactable")
                {
                    Vector3 sawPos = SawTiles.instance.initialPos;

                    tilesData = new TilesData(int.Parse(tileSet.GetChild(i).name), tileSet.transform.childCount, LayerMask.LayerToName(tileSet.GetChild(i).gameObject.layer), tileSet.GetChild(i).tag,
                    tileSet.GetChild(i).name, sawPos.x, sawPos.y,
                    tileSet.GetChild(i).gameObject.transform.localScale.x, tileSet.GetChild(i).gameObject.transform.localScale.y, mapSizeX, mapSizeY);
                }
                else
                {
                    tilesData = new TilesData(int.Parse(tileSet.GetChild(i).name), tileSet.transform.childCount, LayerMask.LayerToName(tileSet.GetChild(i).gameObject.layer), tileSet.GetChild(i).tag,
                    tileSet.GetChild(i).name, tileSet.GetChild(i).gameObject.transform.position.x, tileSet.GetChild(i).gameObject.transform.position.y,
                    tileSet.GetChild(i).gameObject.transform.localScale.x, tileSet.GetChild(i).gameObject.transform.localScale.y, mapSizeX, mapSizeY);
                }
            }
            else
            {
                string tempStrName = tileSet.GetChild(i).name.Substring(0, tileSet.GetChild(i).name.Length - 7);

                tilesData = new TilesData(int.Parse(tempStrName), tileSet.transform.childCount, LayerMask.LayerToName(tileSet.GetChild(i).gameObject.layer), tileSet.GetChild(i).tag,
                tempStrName, tileSet.GetChild(i).gameObject.transform.position.x, tileSet.GetChild(i).gameObject.transform.position.y,
                tileSet.GetChild(i).gameObject.transform.localScale.x, tileSet.GetChild(i).gameObject.transform.localScale.y, mapSizeX, mapSizeY);
            }

            tileDataList.Add(tilesData);
        }
    }

    bool GetInteractables(int index)
    {
        Transform tileSet = GameObject.Find("Tiles").transform;

        if (tileSet.GetChild(index).tag == "Interactable")
        {
            return true;
        }

        return false;
    }

    GameObject AssignObjects(string name)
    {
        GameObject tempGO = null;

        switch (name)
        {
            case "Desert":
                {
                    tempGO = desertSprites;
                }
                break;

            case "Forest":
                {
                    tempGO = forestSprites;
                }
                break;

            case "Winter":
                {
                    tempGO = winterSprites;
                }
                break;

            case "Sci - Fi":
                {
                    tempGO = scifiSprites;
                }
                break;

            case "Objects":
                {
                    tempGO = interactablesSprites;
                }
                break;

            case "Others":
                {
                    tempGO = otherSprites;
                }
                break;
        }

        return tempGO;
    }

    //void AssignInteractablesTag(SpriteRenderer sprite, string environment)
    //{
    //    int[] desert_Interactables = new int[3] { 14, 15, 16 };
    //    int[] forest_Interactables = new int[5] { 13, 14, 15, 17, 18 };
    //    int[] scifi_Interactables = new int[3] { 14, 15, 16 };
    //    int[] winter_Interactables = new int[5] { 14, 15, 16, 17, 18 };
    //    int[] objects_Interactables = new int[3] { 14, 15, 16 };

    //    if (environment == "Desert")
    //    {
    //        SetInteractables(sprite, desert_Interactables);
    //    }
    //    else if (environment == "Forest")
    //    {
    //        SetInteractables(sprite, forest_Interactables);
    //    }
    //    else if (environment == "Sci - Fi")
    //    {
    //        SetInteractables(sprite, scifi_Interactables);
    //    }
    //    else if (environment == "Winter")
    //    {
    //        SetInteractables(sprite, winter_Interactables);
    //    }
    //    else if (environment == "Objects")
    //    {
    //        SetInteractables(sprite, objects_Interactables);
    //    }
    //}

    //void SetObjectLayerMask(SpriteRenderer sr_element)
    //{
    //    switch (sr_element.name)
    //    {
    //        case "1":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "2":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "3":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "7":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "11":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "13":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "14":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "15":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "16":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "20":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "21":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "25":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "26":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "27":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;

    //        case "28":
    //            {
    //                sr_element.gameObject.layer = LayerMask.NameToLayer("Ground");
    //            }
    //            break;
    //    }
    //}

    //void SetInteractables(SpriteRenderer sprite, int[] index)
    //{
    //    for (int i = 0; i < index.Length; i++)
    //    {
    //        if (sprite.name == index[i].ToString())
    //        {
    //            sprite.gameObject.tag = "Interactable";
    //        }
    //    }
    //}

    void RemoveTilesOnGrid()
    {
        if (Input.GetMouseButton(1))        // SPAWNING TILES WHILE HOLDING DOWN LEFT MOUSE BUTTON
        {
            RaycastHit2D hit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 100.0f);

            if (hit2D.collider != null)
            {
                Destroy(hit2D.collider.gameObject);
                //TilesetHandler.instance.GetObstacleTiles(20);
            }
        }
    }

    void UpdateMainCamera()
    {
        // Move Camera Using Middle Mouse Button //

        //if (Input.GetMouseButton(2))
        //{
        //    Camera.main.transform.position += Camera.main.ScreenToWorldPoint(Input.mousePosition) * 0.7f;
        //    Camera.main.transform.position = new Vector3(Camera.main.transform.position.x * 0.5f, Camera.main.transform.position.y * 0.5f, -10f);
        //}
    }

    void SetMapInitials(string MapName, int MapWidth, int MapHeight)
    {
        mapName = MapName;
        mapX = MapWidth;
        mapY = MapHeight;

        PlayerPrefs.SetInt("MAP_WIDTH_SIZE", mapX);
        PlayerPrefs.SetInt("MAP_HEIGHT_SIZE", mapY);
    }

    public int GetMapX()
    {
        return mapX;
    }

    public int GetMapY()
    {
        return mapY;
    }

    public void SetSpawn()
    {
        spawnElement = false;
    }

    public void InstantiateMapElement(Button button)
    {
        // elementSprite.sprite = button.GetComponent<Image>().sprite;
        s_spriteName = button.GetComponent<Image>().sprite.name;
        // TilesetHandler.instance.GetObstacleTiles(20);
        spawnElement = true;
    }

    public void ConfirmButtons(Button button)
    {
        switch (button.name)
        {
            case "ConfirmSaveLoad":
                {
                    if (inputfield.text != "")
                    {
                        MapInitials.SetActive(false);
                        SetMapInitials(inputfield.text, int.Parse(WidthInput.text), int.Parse(HeightInput.text));
                        isMapUpdate = true;

                        foreach (GameObject GO in MapsGO)
                        {
                            GO.gameObject.SetActive(true);
                        }
                    }
                }
                break;

            case "ConfirmMapExpand":
                {
                    isMapExUpdate = true;
                }
                break;

            case "ConfirmLoadMap":
                {
                    LoadCounter++;
                    LoadMapByName.SetActive(false);
                    SaveLoadManager.OpenFile(LoadMapByName.GetComponentInChildren<InputField>().text);
                    mapLoadName = LoadMapByName.GetComponentInChildren<InputField>().text;

                    foreach (GameObject GO in MapsGO)
                    {
                        GO.gameObject.SetActive(true);
                    }

                    for (int i = 0; i < SaveLoadManager.LoadMapData()[0].totalTileSize; i++)
                    {
                        float posX = SaveLoadManager.LoadMapData()[i].tilePosX;
                        float posY = SaveLoadManager.LoadMapData()[i].tilePosY;
                        float sizeX = SaveLoadManager.LoadMapData()[i].tileSizeX;
                        float sizeY = SaveLoadManager.LoadMapData()[i].tileSizeY;
                        float mapSizeX2 = SaveLoadManager.LoadMapData()[i].mapSizeX;
                        float mapSizeY2 = SaveLoadManager.LoadMapData()[i].mapSizeY;

                        string tileTag = SaveLoadManager.LoadMapData()[i].tileTag;
                        string tileName = SaveLoadManager.LoadMapData()[i].tileName;
                        string tileLayer = SaveLoadManager.LoadMapData()[i].tileLayer;

                        gridObj.GetComponent<Grid>().GenerateMap((int)mapSizeX2, (int)mapSizeY2);
                        WidthInput.text = mapSizeX2.ToString();
                        HeightInput.text = mapSizeY2.ToString();

                        PlayerPrefs.SetInt("MAP_WIDTH_SIZE", (int)mapSizeX2);
                        PlayerPrefs.SetInt("MAP_HEIGHT_SIZE", (int)mapSizeY2);

                        GameObject temp = Resources.Load<GameObject>("Prefabs/Custom_Map_Editor/" + tileTag);

                        GameObject tempGO = Instantiate(temp.transform.GetChild(int.Parse(tileName) - 1).gameObject);
                        tempGO.transform.position = new Vector3(posX, posY, -0.9f);
                        tempGO.transform.localScale = new Vector2(sizeX, sizeY);
                        tempGO.transform.SetParent(tilesHolder);

                        if (tempGO.GetComponent<Rigidbody2D>() != null)
                        {
                            tempGO.GetComponent<Rigidbody2D>().isKinematic = true;
                        }
                    }
                }
                break;

            case "CancelLoadMap":
                {
                    LoadMapByName.SetActive(false);
                    MapInitials.SetActive(true);
                }
                break;
        }
    }

    public void SaveData()
    {
        if (inputfield.text != "")
        {
            SetMapInitials(inputfield.text, int.Parse(WidthInput.text), int.Parse(HeightInput.text));

            GetTilesData();
            SaveLoadManager.SaveMap(this, inputfield.text);
        }
        else
        {
            int mapWidthX = int.Parse(WidthInput.text);
            int mapHeightY = int.Parse(HeightInput.text);

            PlayerPrefs.SetInt("MAP_WIDTH_SIZE", mapWidthX);
            PlayerPrefs.SetInt("MAP_HEIGHT_SIZE", mapHeightY);

            SetMapInitials(mapLoadName, PlayerPrefs.GetInt("MAP_WIDTH_SIZE"), PlayerPrefs.GetInt("MAP_HEIGHT_SIZE"));
            GetTilesData();
            SaveLoadManager.SaveMap(this, mapLoadName);
        }
    }

    public void LoadData()
    {
        LoadMapByName.SetActive(true);
        MapInitials.SetActive(false);
    }
}

[Serializable]
public class TilesData
{
    public string tileName { get; set; }
    public string tileTag { get; set; }
    public string tileLayer { get; set; }
    public int tileID { get; set; }
    public int totalTileSize { get; set; }
    public bool isInteractable { get; set; }

    public float tilePosX { get; set; }
    public float tilePosY { get; set; }
    public float tileSizeX { get; set; }
    public float tileSizeY { get; set; }
    public float mapSizeX { get; set; }
    public float mapSizeY { get; set; }

    public TilesData()
    {

    }

    public TilesData(int tileID, int totalTileSize, string tileLayer, string tileTag, string tileName, float tilePosX, float tilePosY, float tileSizeX, float tileSizeY)
    {
        this.tileID = tileID;
        this.tileLayer = tileLayer;
        this.tileTag = tileTag;
        this.tileName = tileName;
        this.tilePosX = tilePosX;
        this.tilePosY = tilePosY;
        this.tileSizeX = tileSizeX;
        this.tileSizeY = tileSizeY;
        this.totalTileSize = totalTileSize;
    }

    public TilesData(int tileID, int totalTileSize, string tileLayer, string tileTag, string tileName, float tilePosX, float tilePosY, float tileSizeX, float tileSizeY, bool isInteractable)
    {
        this.tileID = tileID;
        this.tileLayer = tileLayer;
        this.tileTag = tileTag;
        this.tileName = tileName;
        this.tilePosX = tilePosX;
        this.tilePosY = tilePosY;
        this.tileSizeX = tileSizeX;
        this.tileSizeY = tileSizeY;
        this.totalTileSize = totalTileSize;
        this.isInteractable = isInteractable;
    }

    public TilesData(int tileID, int totalTileSize, string tileLayer, string tileTag, string tileName, float tilePosX,
        float tilePosY, float tileSizeX, float tileSizeY, float mapSizeX, float mapSizeY)
    {
        this.tileID = tileID;
        this.tileLayer = tileLayer;
        this.tileTag = tileTag;
        this.tileName = tileName;
        this.tilePosX = tilePosX;
        this.tilePosY = tilePosY;
        this.tileSizeX = tileSizeX;
        this.tileSizeY = tileSizeY;
        this.totalTileSize = totalTileSize;
        this.isInteractable = isInteractable;
        this.mapSizeX = mapSizeX;
        this.mapSizeY = mapSizeY;
    }
}