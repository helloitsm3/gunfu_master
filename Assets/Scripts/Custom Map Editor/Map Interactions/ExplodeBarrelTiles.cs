﻿using UnityEngine;
using System.Collections;

public class ExplodeBarrelTiles : MonoBehaviour 
{
    [HideInInspector]
    public float knockBack;
    [HideInInspector]
    public int health;

    int initialHealth;
    float respawnTimer;
    float respawnDelay;

    SingletonController sc;
    
	// Use this for initialization
	void Start () 
    {
        sc              = SingletonController.instance;
        health          = 2;
        initialHealth   = health;
        knockBack       = 20.0f;
        respawnTimer    = 0.0f;
        respawnDelay    = 10.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        // BARREL WAITING TO RESPAWN
        if (!gameObject.GetComponent<SpriteRenderer>().enabled)
        {
            respawnTimer += Time.deltaTime;
            Debug.Log(respawnTimer);

            // BARREL RESPAWN
            if (respawnTimer >= respawnDelay)
            {
                respawnTimer = 0.0f;
                gameObject.GetComponent<SpriteRenderer>().enabled = true;
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
                // Z AXIS BUG FIX
                Vector3 pos = gameObject.transform.position;
                gameObject.transform.position = new Vector3(pos.x, pos.y, -pos.z);
            }
        }
	}

    public void damageBarrel(int damageTaken)
    {
        health -= damageTaken;

        if (health <= 0)
        {
            health = initialHealth;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            sc.objectSpawner.spawnExplosions(gameObject.transform);
        }
    }
}
