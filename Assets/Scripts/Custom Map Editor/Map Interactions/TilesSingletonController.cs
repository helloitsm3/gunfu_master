﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TilesSingletonController : MonoBehaviour {

    public static TilesSingletonController instance;

    [HideInInspector]
    public GameObject portal;
    [HideInInspector]
    public List<GameObject> portalList = new List<GameObject>();

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
        portal = GameObject.Find("Tiles");

        for (int i = 0; i < portal.transform.childCount; i++)
        {
            if (portal.transform.GetChild(i).name == "22(Clone)" || portal.transform.GetChild(i).name == "23(Clone)")
            {
                portalList.Add(portal.transform.GetChild(i).gameObject);
            }
        }


        if (portalList.Count > 0)
        {
            // Set ID to TilesName
            if (portalList[0].name == "22(Clone)")
            {
                for (int i = 0; i < portalList.Count; i++)
                {
                    if (portalList[i].name == "22(Clone)")
                        portalList[i].name += i.ToString();
                    if (portalList[i].name == "23(Clone)")
                        portalList[i].name += (i - 1).ToString();
                }
            }
            else if (portalList[0].name == "23(Clone)")
            {
                for (int i = 0; i < portalList.Count; i++)
                {
                    if (portalList[i].name == "22(Clone)")
                        portalList[i].name += (i - 1).ToString();
                    if (portalList[i].name == "23(Clone)")
                        portalList[i].name += (i).ToString();
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
