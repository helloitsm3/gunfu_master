﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TilesInteractHandler : MonoBehaviour 
{
    private GameObject tempGO;
    private bool isInteractable;
    private bool isTurn;
    private bool isSawMove;
    private float duration;
    SingletonController sc;

	// Use this for initialization
	void Start () 
    {
        duration = 3f;
        isInteractable = false;
        isTurn = false;
        isSawMove = false;
        sc = SingletonController.instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    public void ActivateDropInteraction(GameObject fallingGO, Vector2 initialPos)
    {
        if (!sc.resumeGame)
        {
            return;
        }

        if (duration <= 0f && duration > -5f)
        {
            if (isInteractable)
            {
                if (tempGO.GetComponent<Rigidbody2D>() != null)
                {
                    tempGO.GetComponent<BoxCollider2D>().enabled = false;
                    tempGO.GetComponent<Rigidbody2D>().isKinematic = false;
                    //tempGO.transform.position = new Vector3(initialPos.x, initialPos.y, 0);
                }
            }
        }
        else if (duration <= -5f)
        {
            //fallingGO.transform.position = initialPos;
            fallingGO.transform.position = new Vector3(initialPos.x, initialPos.y, -0.9f);
            fallingGO.GetComponent<BoxCollider2D>().enabled = true;
            tempGO.GetComponent<Rigidbody2D>().isKinematic = true;
            isInteractable = false;
            duration = 3f;
        }

        duration -= Time.deltaTime;
    }

    public void ActivatePortal(GameObject portal1, GameObject portal2, GameObject player)
    {
        Vector2 distance = (transform.position - player.transform.position).normalized;

        if (distance.x < 0f)
            player.transform.position = portal2.transform.position - portal2.transform.right * 2;
        else
            player.transform.position = portal2.transform.position + portal2.transform.right * 2;
    }

    public void ActivateSaw(GameObject sawGO, Vector3 initialPos)
    {
        // oldPos - Starting Position
        // newPos - Current Position
        float destination = initialPos.x + 5;
        float xCenter = (destination + initialPos.x) * 0.5f;

        if ((sawGO.transform.position.x <= destination) && !isSawMove)
        {
            sawGO.transform.position += new Vector3(0.05f, 0f);

            if (sawGO.transform.position.x >= destination)
            {
                isSawMove = true;
            }
        }
        else if (isSawMove)
        {
            sawGO.transform.position += new Vector3(-0.05f, 0f);

            if (sawGO.transform.position.x <= initialPos.x)
            {
                isSawMove = false;
            }
        }
    }

    public void ActivateWorm(GameObject wormGO, Vector3 initialPos)
    {
        if (!sc.resumeGame)
        {
            return;
        }

        // oldPos - Starting Position
        // newPos - Current Position
        float destination = initialPos.x + 5;
        float xCenter = (destination + initialPos.x) * 0.5f;

        if ((wormGO.transform.position.x <= destination) && !isTurn)
        {
            wormGO.transform.position += new Vector3(0.05f, 0f);

            if (wormGO.transform.position.x >= destination)
            {
                wormGO.transform.localScale = new Vector3(wormGO.transform.localScale.x * -1, wormGO.transform.localScale.y, 0);
                isTurn = true;
            }
        }
        else if (isTurn)
        {
            wormGO.transform.position += new Vector3(-0.05f, 0f);

            if (wormGO.transform.position.x <= initialPos.x)
            {
                wormGO.transform.localScale = new Vector3(wormGO.transform.localScale.x * -1, wormGO.transform.localScale.y, 0);
                isTurn = false;
            }
        }
    }

    public void SetInteractable(GameObject tempGO, bool isInteractable)
    {
        this.isInteractable = isInteractable;
        this.tempGO = tempGO;
    }

    public bool GetInteractable()
    {
        return isInteractable;
    }
}
