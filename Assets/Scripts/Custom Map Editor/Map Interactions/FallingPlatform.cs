﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : MonoBehaviour
{
    private Vector2 initialPos;

	// Use this for initialization
	void Start () {
        initialPos = this.transform.position;
	}   
	
	// Update is called once per frame
    void Update()
    {
        if (GetComponent<TilesInteractHandler>() != null)
        {
            if (GetComponent<TilesInteractHandler>().GetInteractable())
            {
                GetComponent<TilesInteractHandler>().ActivateDropInteraction(this.gameObject, initialPos);
            }
        }
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        this.gameObject.transform.position = new Vector3(initialPos.x, initialPos.y, 0);
    }
}
