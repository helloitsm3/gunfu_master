﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PortalTiles : MonoBehaviour {

    public GameObject OtherPortal;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name + "  " + collision.gameObject.tag);

        if (GetComponent<TilesInteractHandler>() != null && collision.gameObject.tag != "Others")
        {
            for (int i = 0; i < TilesSingletonController.instance.portalList.Count; i++)
            {
                if (this.transform.name != TilesSingletonController.instance.portalList[i].name && this.transform.name.Substring(9, this.transform.name.Length - 9) == 
                    TilesSingletonController.instance.portalList[i].name.Substring(9, this.transform.name.Length - 9))
                {
                    GetComponent<TilesInteractHandler>().ActivatePortal(transform.gameObject, TilesSingletonController.instance.portalList[i], collision.gameObject);
                }
            }
        }
    }
}