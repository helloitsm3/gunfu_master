﻿using UnityEngine;
using System.Collections;

public class SawTiles : MonoBehaviour 
{
    public static SawTiles instance;
    float knockBack;
    public Vector3 initialPos;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () 
    {
        knockBack = 15.0f;
        initialPos = this.gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (GetComponent<TilesInteractHandler>() != null)
        {
            GetComponent<TilesInteractHandler>().ActivateSaw(this.gameObject, initialPos);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            // CHECK WHETHER PLAYER HAS INVULNERABILITY
            if (!other.GetComponent<PlayerController>().invulnerable)
            {
                Rigidbody2D rb = other.gameObject.GetComponent<Rigidbody2D>();
                Vector3 direction = (transform.position - other.transform.position).normalized;

                // SET FORCE DIRECTION
                rb.velocity = Vector2.zero;
                if (direction.x > 0.0f)
                {
                    //Debug.Log(other.name + " goes left");
                    rb.velocity = new Vector2(-knockBack, Mathf.Abs(knockBack));
                }

                else
                {
                    //Debug.Log(other.name + " goes right");
                    rb.velocity = new Vector2(knockBack, Mathf.Abs(knockBack));
                }

                other.GetComponent<PlayerController>().knockedBack = true;
            }

            // PLAYER UNAFFECTED DUE TO INVULNERABILITY
            else
            {
                other.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
        }
    }
}
