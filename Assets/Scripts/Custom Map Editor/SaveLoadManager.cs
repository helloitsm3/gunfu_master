﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;

public static class SaveLoadManager
{
    private static List<TilesData> MapList = new List<TilesData>();

    public static void SaveMap(CustomMapEditor CME, string fileName)
    {
        string filePath = "/Resources/Maps/" + fileName + ".punchclub";
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(Application.dataPath + filePath, FileMode.Create);
        MapData mapData = new MapData(CME);

        formatter.Serialize(stream, mapData);
        stream.Close();
    }

    public static void LoadMap(string fileName)
    {
        string filePath = "/Resources/Maps/" + fileName + ".punchclub";

        if (File.Exists(Application.dataPath + filePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(Application.dataPath + filePath, FileMode.Open);
            MapData mapData = (MapData)formatter.Deserialize(stream);

            stream.Close();
        }
        else
        {
            Debug.LogError("FILE DOESN'T EXIST!");
        }
    }

    public static void OpenFile(string fileName)
    {
        string filePath = "/Resources/Maps/" + fileName + ".punchclub";
        if (File.Exists(Application.dataPath + filePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(Application.dataPath + filePath, FileMode.Open);
            MapData mapData = (MapData)formatter.Deserialize(stream);

            for (int i = 0; i < mapData.tileList.Count; i++)
            {
                MapList.Add(mapData.tileList[i]);
            }

            stream.Close();
        }
        else
        {
            Debug.LogError("FILE DOESN'T EXIST!");
        }
    }

    public static List<TilesData> LoadMapData()
    {
        return MapList;
    }

    public static List<TilesData> LoadMapList(string fileName)
    {
        string filePath = "/Resources/Maps/" + fileName + ".punchclub";

        if (File.Exists(Application.dataPath + filePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(Application.dataPath + filePath, FileMode.Open);
            MapData mapData = (MapData)formatter.Deserialize(stream);

            stream.Close();

            return mapData.tileList;
        }
        else
        {
            Debug.LogError("FILE DOESN'T EXIST!");
            return null;
        }
    }
}

[Serializable]
public class MapData
{
    public string mapName = "";
    public int mapDataSizeX = 0, mapDataSizeY = 0;
    public List<TilesData> tileList = new List<TilesData>();
    public float tilePosX, tilePosY;

    public MapData(CustomMapEditor CME)
    {
        mapDataSizeX = CME.mapX;
        mapDataSizeY = CME.mapY;
        mapName = CME.mapName;

        for (int i = 0; i < CME.tileDataList.Count; i++)
        {
            tileList.Add(new TilesData(CME.tileDataList[i].tileID,  CME.tileDataList[i].totalTileSize, CME.tileDataList[i].tileLayer, CME.tileDataList[i].tileTag, CME.tileDataList[i].tileName, 
                CME.tileDataList[i].tilePosX, CME.tileDataList[i].tilePosY, CME.tileDataList[i].tileSizeX, CME.tileDataList[i].tileSizeY, CME.tileDataList[i].mapSizeX, CME.tileDataList[i].mapSizeY));
        }
    }
}