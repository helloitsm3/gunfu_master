﻿using UnityEngine;
using System.Collections;

public class MouseBoundaryCheck : MonoBehaviour
{
    public new Camera camera;
    public int Boundary = 50; // distance from edge scrolling starts
    public int speed = 5;

    private int theScreenWidth;
    private int theScreenHeight;

    // Use this for initialization
    void Start()
    {
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;
    }

    // Update is called once per frame
    void Update()
    {
        // HOLDING KEY TO TRIGGER CAMERA MOVEMENT
        if (Input.GetMouseButton(2))
        {
            if (Input.mousePosition.x > theScreenWidth - Boundary)
            {
                camera.transform.position += new Vector3(speed * Time.deltaTime, 0); // move on +X axis
            }
            if (Input.mousePosition.x < 0 + Boundary)
            {
                camera.transform.position -= new Vector3(speed * Time.deltaTime, 0); // move on -X axis
            }
            if (Input.mousePosition.y > theScreenHeight - Boundary)
            {
                camera.transform.position += new Vector3(0, speed * Time.deltaTime); // move on +Z axis
            }
            if (Input.mousePosition.y < 0 + Boundary)
            {
                camera.transform.position -= new Vector3(0, speed * Time.deltaTime);  // move on -Z axis
            }
        }
    }

    public void ButtonPointerEnter()
    {
        CustomMapEditor.instance.spawnElement = false;
    }
}
