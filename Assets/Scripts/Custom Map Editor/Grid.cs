﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

    [Range(0, 1)]
    public float outlinePercent;
    public Transform tilePrefab;
    public Vector2 mapSize;
    public GameObject CMEditor;

    void Start()
    {
        GenerateMap();
    }

    void Update()
    {
        if (CMEditor.GetComponent<CustomMapEditor>().isMapUpdate)
        {
            mapSize = new Vector3(CMEditor.GetComponent<CustomMapEditor>().GetMapX(), CMEditor.GetComponent<CustomMapEditor>().GetMapY());
            GenerateMap();
            CMEditor.GetComponent<CustomMapEditor>().isMapUpdate = false;
        }
    }

    public void GenerateMap ()
    {
        string holderName = "Generated Map";

        if(transform.Find(holderName))
        {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.parent = transform;

        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                Vector3 tilePosition = new Vector3(-mapSize.x / 2 + 0.5f + x, -mapSize.y / 2 + 0.5f + y, 0.0f);
                Transform newTile = (Transform)Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right));

                newTile.localScale = Vector3.one * (1 - outlinePercent);
                newTile.SetParent(mapHolder.transform);
            }
        }
    }

    public void GenerateMap(int width, int height)
    {
        string holderName = "Generated Map";

        if (transform.Find(holderName))
        {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.parent = transform;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Vector3 tilePosition = new Vector3(-width / 2 + 0.5f + x, -height / 2 + 0.5f + y, 0.0f);
                Transform newTile = (Transform)Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right));

                newTile.localScale = Vector3.one * (1 - outlinePercent);
                newTile.SetParent(mapHolder.transform);
            }
        }
    }
}
