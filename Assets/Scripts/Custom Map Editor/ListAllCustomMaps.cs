﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ListAllCustomMaps : MonoBehaviour
{

    public GameObject contentParent;
    public GameObject mapDisplayUI;
    public GameObject ScrollViewContent;

    private static DirectoryInfo levelDirectoryPath;
    private FileInfo[] fileinfo;
    private string fileExtension = ".punchclub";
    private float SCREEN_WIDTH;
    private float SCREEN_HEIGHT;

    // Use this for initialization
    void Start()
    {
        ResizeCanvas();
        levelDirectoryPath = new DirectoryInfo(Application.dataPath);
        fileinfo = levelDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);

        foreach (FileInfo file in fileinfo)
        {
            if (file.Extension == fileExtension)
            {
                if (file.Name != "Forest.punchclub" && file.Name != "desert.punchclub"
                    && file.Name != "sci - fi.punchclub" && file.Name != "winter.punchclub"
                    && file.Name != "Lucky Bomb.punchclub")
                {
                    string fileName = file.Name.Substring(0, file.Name.Length - 10);
                    GameObject temp = Instantiate(mapDisplayUI);
                    temp.transform.SetParent(contentParent.transform);
                    temp.transform.GetChild(0).GetComponentInChildren<Text>().text = fileName;
                }
            }
        }
    }

    void ResizeCanvas()
    {
        //SCREEN_WIDTH = this.gameObject.GetComponent<RectTransform>().rect.width;
        //SCREEN_HEIGHT = 100.0f;
        //Vector2 newSize = new Vector2(SCREEN_WIDTH, SCREEN_HEIGHT);
        //ScrollViewContent.GetComponent<GridLayoutGroup>().cellSize = newSize;
    }

    // Update is called once per frame
    void Update()
    {

    }
}

