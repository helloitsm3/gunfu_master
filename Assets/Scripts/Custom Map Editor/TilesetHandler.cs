﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TilesetHandler : MonoBehaviour
{
    public static TilesetHandler instance;
    public GameObject AnimatedOBJ;
    public GameObject tileselectionPanel;

    [HideInInspector]
    public string b_Name;

    public Sprite[] DesertTiles;
    public Sprite[] ForestTiles;
    public Sprite[] SciFiTiles;
    public Sprite[] WinterTiles;
    public Sprite[] ObstaclesTiles;
    public Sprite[] FallingPlatformTiles;

    private Button[] TileSelector;
    private bool b_isActive;

    void Awake()
    {
        instance = this;
    }

	void Start () {
        AnimatedOBJ.GetComponent<Animator>().enabled = false;
        b_isActive = false;
        b_Name = "";

        if (tileselectionPanel != null)
        {
            TileSelector = new Button[tileselectionPanel.transform.childCount];

            for (int i = 0; i < tileselectionPanel.transform.childCount; i++)
            {
                TileSelector[i] = (Button)tileselectionPanel.transform.GetChild(i).GetComponent<Button>();
                TileSelector[i].gameObject.SetActive(false);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        UpdateButtonActive();
	}

    void UpdateButtonActive()
    {
        if (b_isActive)
        {
            switch (b_Name)
            {
                case "Desert":
                    {
                        SetButtonActive(16);

                        for (int i = 0; i < DesertTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = DesertTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 5;
                        }
                    }
                    break;

                case "Forest":
                    {
                        SetButtonActive(18);

                        for (int i = 0; i < ForestTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = ForestTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 5;
                        }
                    }
                    break;

                case "Sci - Fi":
                    {
                        SetButtonActive(28);

                        for (int i = 0; i < SciFiTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = SciFiTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 5;
                        }
                    }
                    break;

                case "Winter":
                    {
                        SetButtonActive(18);

                        for (int i = 0; i < WinterTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = WinterTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 5;
                        }
                    }
                    break;

                case "Objects":
                    {
                        SetButtonActive(34);

                        for(int i = 0; i < ObstaclesTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = ObstaclesTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(100, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 5;
                        }
                    }
                    break;

                case "Others":
                    {
                        SetButtonActive(6);
                        for (int i = 0; i < FallingPlatformTiles.Length; i++)
                        {
                            TileSelector[i].GetComponent<Image>().sprite = FallingPlatformTiles[i];
                            tileselectionPanel.GetComponent<GridLayoutGroup>().cellSize = new Vector2(500, 100);
                            tileselectionPanel.GetComponent<GridLayoutGroup>().constraintCount = 1;
                        }
                    }
                    break;
            }
        }
    }

    void SetButtonActive(int index)
    {
        for (int i = 0; i < TileSelector.Length; i++)
        {
            if (i < index)
            {
                TileSelector[i].gameObject.SetActive(true);
            }
            else
            {
                TileSelector[i].gameObject.SetActive(false);
                b_isActive = false;
            }
        }
    }

    public void ReverseAnimation()
    {
        AnimatedOBJ.GetComponent<Animator>().SetTrigger("close_tilesMenu");
    }

    public void PlayAnimation(Button button)
    {
        b_Name = button.GetComponentInChildren<Text>().text;
        b_isActive = true;
        AnimatedOBJ.GetComponent<Animator>().enabled = true;
        AnimatedOBJ.GetComponent<Animator>().SetTrigger("open_tilesMenu");
    }

    public void GetObstacleTiles(int index)
    {
        if (b_Name == "Objects")
        {
            if (TileSelector[index].gameObject.activeInHierarchy)
            {
                TileSelector[index].gameObject.SetActive(false);
            }
            else
            {
                TileSelector[index].gameObject.SetActive(true);
            }
        }
    }
}
