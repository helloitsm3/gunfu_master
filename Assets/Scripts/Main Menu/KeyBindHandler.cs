﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class KeyBindHandler : MonoBehaviour {

    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    private GameObject currentKey;
    private Color32 normalColor = Color.white;
    private Color32 selectedColor = new Color32(239, 116, 36, 255);

    private string[] playerTag = { "Player 1 ", "Player 2 ", "Player 3 ", "Player 4 " };
    private string parentName;

    public Text p1_left, p1_right, p1_jump, p1_ultimate, p1_melee, p1_shoot, p1_weapon_switch;
    public Text p2_left, p2_right, p2_jump, p2_ultimate, p2_melee, p2_shoot, p2_weapon_switch;
    public Text p3_left, p3_right, p3_jump, p3_ultimate, p3_melee, p3_shoot, p3_weapon_switch;
    public Text p4_left, p4_right, p4_jump, p4_ultimate, p4_melee, p4_shoot, p4_weapon_switch;

	// Use this for initialization
	void Start ()
    {
        keys.Add("P1_Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Left Button", "A")));
        keys.Add("P1_Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Right Button", "D")));
        keys.Add("P1_Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Jump Button", "W")));
        keys.Add("P1_Ultimate", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Ultimate Button", "S")));
        keys.Add("P1_Melee", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Melee Button", "Q")));
        keys.Add("P1_Shoot", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Shoot Button", "E")));
        keys.Add("P1_Weapon_Switch", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 1 Weapon Switch Button", "E")));

        keys.Add("P2_Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Left Button", "G")));
        keys.Add("P2_Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Right Button", "J")));
        keys.Add("P2_Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Jump Button", "Y")));
        keys.Add("P2_Ultimate", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Ultimate Button", "H")));
        keys.Add("P2_Melee", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Melee Button", "T")));
        keys.Add("P2_Shoot", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Shoot Button", "U")));
        keys.Add("P2_Weapon_Switch", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 2 Weapon Switch Button", "E")));

        //keys.Add("P3_Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Left Button", "Z")));
        //keys.Add("P3_Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Right Button", "X")));
        //keys.Add("P3_Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Jump Button", "C")));
        //keys.Add("P3_Ultimate", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Ultimate Button", "V")));
        //keys.Add("P3_Melee", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Melee Button", "O")));
        //keys.Add("P3_Shoot", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Shoot Button", "P")));
        //keys.Add("P3_Weapon_Switch", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 3 Weapon Switch Button", "E")));

        //keys.Add("P4_Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Left Button", "1")));
        //keys.Add("P4_Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Right Button", "2")));
        //keys.Add("P4_Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Jump Button", "3")));
        //keys.Add("P4_Ultimate", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Ultimate Button", "4")));
        //keys.Add("P4_Melee", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Melee Button", "5")));
        //keys.Add("P4_Shoot", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Shoot Button", "6")));
        //keys.Add("P4_Weapon_Switch", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Player 4 Weapon Switch Button", "E")));


        p1_left.text            = keys["P1_Left"].ToString();
        p1_right.text           = keys["P1_Right"].ToString();
        p1_jump.text            = keys["P1_Jump"].ToString();
        p1_ultimate.text        = keys["P1_Ultimate"].ToString();
        p1_melee.text           = keys["P1_Melee"].ToString();
        p1_shoot.text           = keys["P1_Shoot"].ToString();
        p1_weapon_switch.text   = keys["P1_Weapon_Switch"].ToString();

        p2_left.text            = keys["P2_Left"].ToString();
        p2_right.text           = keys["P2_Right"].ToString();
        p2_jump.text            = keys["P2_Jump"].ToString();
        p2_ultimate.text        = keys["P2_Ultimate"].ToString();
        p2_melee.text           = keys["P2_Melee"].ToString();
        p2_shoot.text           = keys["P2_Shoot"].ToString();
        p2_weapon_switch.text   = keys["P2_Weapon_Switch"].ToString();

        //p3_left.text            = keys["P3_Left"].ToString();
        //p3_right.text           = keys["P3_Right"].ToString();
        //p3_jump.text            = keys["P3_Jump"].ToString();
        //p3_ultimate.text        = keys["P3_Ultimate"].ToString();
        //p3_melee.text           = keys["P3_Melee"].ToString();
        //p3_shoot.text           = keys["P3_Shoot"].ToString();
        //p3_weapon_switch.text   = keys["P3_Weapon_Switch"].ToString();

        //p4_left.text            = keys["P4_Left"].ToString();
        //p4_right.text           = keys["P4_Right"].ToString();
        //p4_jump.text            = keys["P4_Jump"].ToString();
        //p4_ultimate.text        = keys["P4_Ultimate"].ToString();
        //p4_melee.text           = keys["P4_Melee"].ToString();
        //p4_shoot.text           = keys["P4_Shoot"].ToString();
        //p4_weapon_switch.text   = keys["P4_Weapon_Switch"].ToString();
	}    
	
	// Update is called once per frame
	void Update ()
    {
	}

    void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;

            if (e.isKey)
            {
                for (int i = 0; i < playerTag.Length; i++)
                {
                    if (playerTag[i] == parentName)
                    {
                        keys[playerTag[i] + currentKey.name] = e.keyCode;
                        currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                        currentKey.GetComponent<Image>().color = normalColor;
                        currentKey = null;
                    }
                }
            }
        }
    }

    void ErrorCheck()
    {

    }

    public void ChangeKey(GameObject clickedObject)
    {
        if (currentKey != null)
        {
            currentKey.GetComponent<Image>().color = normalColor;
        }

        currentKey = clickedObject;
        currentKey.GetComponent<Image>().color = selectedColor;
        parentName = clickedObject.transform.parent.parent.parent.name.Substring(0, clickedObject.transform.parent.parent.parent.name.Length - 5);
    }

    public void SaveKeyBinds()
    {
        foreach (KeyValuePair<string, KeyCode> keyDict in keys)
        {
            Debug.Log(keyDict.Key + "    " + keyDict.Value.ToString());
            PlayerPrefs.SetString(keyDict.Key, keyDict.Value.ToString());
        }

        PlayerPrefs.Save();
    }
}
