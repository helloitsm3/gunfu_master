﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class CharSelectPage : MonoBehaviour
{
    public Image[] DualPreview;
    public Image[] PlayerMarkers;
    public Text PCountText;
    public Text player1_name;
    public Text player2_name;
    public GameObject[] mouseDragSelection;

    private bool isKeyInput;
    private bool isKeyPressed;
    private int PlayerCounter;
    private int key_count, key_count2;
    private GameObject CharSelectObj;
    private GameObject[] charChildArray;
    
    private Vector3[] originalPos;
    int checkWhichKey;

    // Use this for initialization
    void Start()
    {
        PlayerCounter = 2;
        key_count = 0;
        key_count2 = 0;
        isKeyInput = true;
        isKeyPressed = false;

        player1_name.text = "Cassidy";
        player2_name.text = "Cassidy";

        CharSelectObj = GameObject.Find("Character Selection");
        PCountText.text = PlayerCounter.ToString();
        charChildArray = new GameObject[CharSelectObj.transform.childCount];
        originalPos = new Vector3[DualPreview.Length];

        for (int i = 0; i < CharSelectObj.transform.childCount; i++)
        {
            charChildArray[i] = CharSelectObj.transform.GetChild(i).gameObject;
        }

        foreach (GameObject obj in mouseDragSelection)
        {
            obj.SetActive(false);
        }

        foreach (Image img in PlayerMarkers)
        {
            img.transform.SetParent(charChildArray[0].transform);
            img.transform.position = new Vector3(0, 0, 0);
        }

        foreach (Image img in DualPreview)
        {
            img.gameObject.SetActive(true);
            img.sprite = charChildArray[key_count].GetComponent<Image>().sprite;
        }

        PlayerMarkers[0].transform.SetParent(charChildArray[key_count].transform);
        PlayerMarkers[0].transform.localPosition = new Vector3(0, 0, 0);
        PlayerMarkers[0].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

        PlayerMarkers[1].transform.SetParent(charChildArray[key_count2].transform);
        PlayerMarkers[1].transform.localPosition = new Vector3(0, 0, 0);
        PlayerMarkers[1].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
        SelectCurrentChar();

        for (int i = 0; i < DualPreview.Length; i++)
        {
            originalPos[i] = DualPreview[i].transform.position;
        }
    }

    void LateUpdate()
    {
        for (int i = 0; i < DualPreview.Length; i++)
        {
            DualPreview[i].transform.position = originalPos[i];
            DualPreview[i].GetComponent<Image>().SetNativeSize();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isKeyInput)
            SelectCurrentChar();
    }

    public void IncrementCounter()
    {
        PlayerCounter++;

        if (PlayerCounter > 4)
        {
            PlayerCounter = 1;
        }

        ActivateDualPreview(PlayerCounter);

        PCountText.text = PlayerCounter.ToString();
    }

    public void DecrementCounter()
    {
        PlayerCounter--;

        if (PlayerCounter < 1)
        {
            PlayerCounter = 4;
        }

        ActivateDualPreview(PlayerCounter);

        PCountText.text = PlayerCounter.ToString();
    }

    public void SelectionStyleButton(Button button)
    {
        Text bText = button.transform.GetChild(0).GetComponent<Text>();

        if (bText.text == "Mouse-Drag")
        {
            bText.text = "Key-Input";
            isKeyInput = true;
            key_count = 0;

            foreach (GameObject obj in mouseDragSelection)
            {
                obj.SetActive(false);
            }

            for (int i = 0; i < CharSelectObj.transform.childCount; i++)
            {
                charChildArray[i].GetComponent<Button>().interactable = true;
            }
        }
        else if (bText.text == "Key-Input")
        {
            bText.text = "Mouse-Drag";
            isKeyInput = false;
            key_count = 0;

            foreach (GameObject obj in mouseDragSelection)
            {
                obj.SetActive(true);
            }

            for (int i = 0; i < CharSelectObj.transform.childCount; i++)
            {
                charChildArray[i].GetComponent<Button>().interactable = false;
            }
        }
    }

    void ActivateDualPreview(int counter)
    {
        if (counter == 2)
        {
            foreach (Image img in DualPreview)
            {
                img.gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (Image img in DualPreview)
            {
                img.gameObject.SetActive(false);
            }
        }

        for(int i = 0; i < counter; i++)
        {
            if(i < counter)
                mouseDragSelection[1].GetComponent<Transform>().GetChild(i).gameObject.SetActive(true);
            else
                mouseDragSelection[1].GetComponent<Transform>().GetChild(i).gameObject.SetActive(false);
        }
    }

    void SelectCurrentChar()
    {
        if (Input.anyKey)
            isKeyPressed = true;
        else
            isKeyPressed = false;

        // CHECKING OF KEYBOARD INPUT
        if (isKeyPressed)
        {
            foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (kcode.ToString() == PlayerPrefs.GetString("Player 1 Left Button"))
                {
                    if (Input.GetKeyDown(kcode))
                    {
                        AudioSingleton.instance.PlayButtonClick();
                        key_count--;
                    }
                }
                
                else if (kcode.ToString() == PlayerPrefs.GetString("Player 1 Right Button"))
                {
                    if (Input.GetKeyDown(kcode))
                    {
                        AudioSingleton.instance.PlayButtonClick();
                        key_count++;
                    }
                }

                if (kcode.ToString() == PlayerPrefs.GetString("Player 2 Left Button"))
                {
                    if (Input.GetKeyDown(kcode))
                    {
                        AudioSingleton.instance.PlayButtonClick();
                        key_count2--;
                    }
                }
                
                else if (kcode.ToString() == PlayerPrefs.GetString("Player 2 Right Button"))
                {
                    if (Input.GetKeyDown(kcode))
                    {
                        AudioSingleton.instance.PlayButtonClick();
                        key_count2++;
                    }
                }
            }
        }

        // CONTROLLER INPUT
        else
        {
            if (Input.GetJoystickNames().Length >= 1)
            {
                key_count = ControllerSingleton.instance.characterSelectInputDPad(1, key_count);
                if (Input.GetJoystickNames().Length == 2)
                {
                    key_count2 = ControllerSingleton.instance.characterSelectInputDPad(2, key_count2);
                }
            }
        }      

        if (key_count < 0)
            key_count = CharSelectObj.transform.childCount - 1;
        else if (key_count >= CharSelectObj.transform.childCount)
            key_count = 0;

        if (key_count2 < 0)
            key_count2 = CharSelectObj.transform.childCount - 1;
        else if (key_count2 >= CharSelectObj.transform.childCount)
            key_count2 = 0;

        PlayerMarkers[0].transform.SetParent(charChildArray[key_count].transform);
        PlayerMarkers[0].transform.localPosition = new Vector3(0, 0, 0);
        PlayerMarkers[0].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

        PlayerMarkers[1].transform.SetParent(charChildArray[key_count2].transform);
        PlayerMarkers[1].transform.localPosition = new Vector3(0, 0, 0);
        PlayerMarkers[1].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

        SetCurrentCharName(key_count, key_count2);

        PlayerPrefs.SetInt("Player 1 Index", key_count);
        PlayerPrefs.SetInt("Player 2 Index", key_count2);

        playAnimation();
    }

    void SetCurrentCharName(int index1, int index2)
    {
        switch(index1)
        {
            case 0:
                {
                    player1_name.text = "Cassidy";
                }
                break;

            case 1:
                {
                    player1_name.text = "Gaiden";
                }
                break;

            case 2:
                {
                    player1_name.text = "Elliot";
                }
                break;

            case 3:
                {
                    player1_name.text = "Hambe";
                }
                break;
        }

        switch (index2)
        {
            case 0:
                {
                    player2_name.text = "Cassidy";
                }        
                break;   
                         
            case 1:      
                {        
                    player2_name.text = "Gaiden";
                }         
                break;    
                          
            case 2:       
                {         
                    player2_name.text = "Elliot";
                }        
                break;   
                         
            case 3:      
                {        
                    player2_name.text = "Hambe";
                }
                break;
        }
    }

    void playAnimation()
    {
        // RUN ANIMATION
        DualPreview[0].gameObject.GetComponent<Animator>().Play("Char" + key_count.ToString() + "_Win");
        DualPreview[1].gameObject.GetComponent<Animator>().Play("Char" + key_count2.ToString() + "_Win");

        // ADJUST ANIMATION SIZE
        for (int i = 0; i < DualPreview.Length; i++)
        {
            if (i == 0)
            {
                checkWhichKey = key_count;
            }

            else if (i == 1)
            {
                checkWhichKey = key_count2;
            }

            if (checkWhichKey == 1)
            {
                if (DualPreview[i].transform.localScale.x > 0.0f)
                {
                    DualPreview[i].transform.localScale = new Vector3(1.1f, 1.1f);
                }

                else
                {
                    DualPreview[i].transform.localScale = new Vector3(-1.1f, 1.1f);
                }
            }

            else if (checkWhichKey == 3)
            {
                if (DualPreview[i].transform.localScale.x > 0.0f)
                {
                    DualPreview[i].transform.localScale = new Vector3(0.85f, 0.85f);
                }

                else
                {
                    DualPreview[i].transform.localScale = new Vector3(-0.85f, 0.85f);
                }
            }

            else
            {
                if (DualPreview[i].transform.localScale.x > 0.0f)
                {
                    DualPreview[i].transform.localScale = new Vector3(1.0f, 1.0f);
                }

                else
                {
                    DualPreview[i].transform.localScale = new Vector3(-1.0f, 1.0f);
                }
            }
        }  
    }
}
