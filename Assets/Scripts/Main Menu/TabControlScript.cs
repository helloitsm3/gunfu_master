﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabControlScript : MonoBehaviour {

    public Button[] optionButtons;
    public GameObject[] optionSettings;

    // Use this for initialization
    void Start()
    {
        foreach (GameObject GO in optionSettings)
        {
            if (GO.name != "KeyBinds")
                GO.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void DisplaySettings(Button button)
    {
        foreach (GameObject GO in optionSettings)
        {
            if (button.name == GO.name)
            {
                GO.SetActive(true);
            }
            else
            {
                GO.SetActive(false);
            }   
        }
    }
}
