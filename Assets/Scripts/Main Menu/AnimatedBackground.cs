﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AnimatedBackground : MonoBehaviour
{

    public List<UnityEngine.Video.VideoPlayer> movieTextures;
    public List<UnityEngine.Video.VideoClip> movieTextures2;

    // Use this for initialization
    void Start()
    {
        int randomCount = Random.Range(0, movieTextures.Count);

        movieTextures[randomCount].clip = movieTextures2[randomCount];
        movieTextures[randomCount].playOnAwake = true;
        GetComponent<RawImage>().texture = movieTextures[randomCount].texture;
        movieTextures[randomCount].Play();
        movieTextures[randomCount].isLooping = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
