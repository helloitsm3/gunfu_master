﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectHandler : MonoBehaviour 
{
    public ScrollRect scrollRect;
    public RectTransform contentPanel;
    public Image[] image;
    public GameObject ScrollViewContent;
    public GameObject CustomMapView;

    private int index;
    private bool isScrolled;
    private float SCREEN_WIDTH;
    private float SCREEN_HEIGHT;
    private float elapsedTime;
    private float isDisabledTime;
    private RectTransform tempTarg;

	// Use this for initialization
	void Start () 
    {
        elapsedTime = 0f;
        isDisabledTime = 0f;
        index = 0;

        CustomMapView.GetComponent<Animator>().enabled = false;

        PlayerPrefs.SetString("Loading_Map_Name", "forest");
        PlayerPrefs.Save();

        isScrolled = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        ResizeCanvas();

        if (isScrolled)
            CanvasSnap();
	}

    void ResizeCanvas()
    {
        SCREEN_WIDTH = this.gameObject.GetComponent<RectTransform>().rect.width;
        SCREEN_HEIGHT = this.gameObject.GetComponent<RectTransform>().rect.height;
        Vector2 newSize = new Vector2(SCREEN_WIDTH, SCREEN_HEIGHT);
        ScrollViewContent.GetComponent<GridLayoutGroup>().cellSize = newSize;
    }

    public void SnapTo()
    {
        tempTarg = image[GetButtonIndex()].rectTransform;
        isScrolled = true;
    }

    private void CanvasSnap()
    {
        elapsedTime += Time.deltaTime;

        Canvas.ForceUpdateCanvases();
        contentPanel.anchoredPosition = Vector3.Lerp((Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position),
            (Vector2)scrollRect.transform.InverseTransformPoint(contentPanel.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(tempTarg.position), elapsedTime);

        if (elapsedTime >= 1.0f)
        {
            isScrolled = false;
            elapsedTime = 0f;
        }
    }

    public void SetButtonIndex(int index)
    {
        this.index = index;

        if (index == UIManagerScript.instance.levels.transform.childCount - 1)
        {
            PlayerPrefs.SetString("CHECKCUSTOM", "true");
        }

        else
        {
            PlayerPrefs.SetString("CHECKCUSTOM", "false");
        }
    }

    public int GetButtonIndex()
    {
        return index;
    }

    public void EnableCustomMapView()
    {
        CustomMapView.GetComponent<Animator>().enabled = true;
        CustomMapView.GetComponent<Animator>().SetTrigger("enableCustom");
        isDisabledTime = 0;
    }

    public void DisableCustomMapView()
    {
        if (isDisabledTime == 0.0f)
        {
            CustomMapView.GetComponent<Animator>().SetTrigger("disableCustom");
            isDisabledTime += Time.deltaTime;

            if (isDisabledTime >= 1.0f)
            {
                CustomMapView.GetComponent<Animator>().enabled = false;
            }
        }
    }

    public void LoadMapByName(string name)
    {
        PlayerPrefs.SetString("Loading_Map_Name", name);
        PlayerPrefs.Save();
    }
}