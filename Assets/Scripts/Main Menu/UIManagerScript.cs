﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour
{
    public GameObject[] PageList;
    public Image[] CharPreview;
    public static UIManagerScript instance;
    public GameObject buttonsGroup;
    public GameObject levels;
    public GameObject gameModes;

    [HideInInspector]
    public bool customMapSelected;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        LoadPageByName("MainMenuPage");
        customMapSelected = false;
        // SET DEFAULT GAMEMODE
        setGameMode("DeathMatch");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
            QuitGame();

        // CODE TO REVERSE ANIMATION
        //if (Input.GetKey(KeyCode.W))
        //{
        //    GetComponent<Animator>().SetFloat("f_animation_speed", -1);
        //    GetComponent<Animator>().speed = 1;
        //}

        // CHECK IF ANY CONTROLLER IS CONNECTED
        if (Input.GetJoystickNames().Length >= 1)
        {
            // MAIN MENU PAGE CURRENTLY ACTIVE
            if (PageList[0].activeInHierarchy)
            {
                ControllerSingleton.instance.mainMenuInputDPad(1, buttonsGroup);
                ControllerSingleton.instance.mainMenuInputButtons(1, buttonsGroup);
            }

            // OPTION PAGE CURRENTLY ACTIVE
            else if (PageList[1].activeInHierarchy)
            {
                ControllerSingleton.instance.backButton(1, "MainMenuPage");
            }

            // LEVEL SELECT PAGE CURRENTLY ACTIVE
            else if (PageList[2].activeInHierarchy)
            {
                ControllerSingleton.instance.gameModeSelectInputDPad(1, gameModes);
                ControllerSingleton.instance.levelSelectInputDPad(1, levels, PageList[2].GetComponent<LevelSelectHandler>());
                ControllerSingleton.instance.levelSelectInputButtons(1, levels);
                ControllerSingleton.instance.backButton(1, "CharSelectPage");
            }

            // CHARACTER SELECT PAGE CURRENTLY ACTIVE
            else if (PageList[3].activeInHierarchy)
            {
                ControllerSingleton.instance.comfirmButton(1, "LevelSelectPage");
                ControllerSingleton.instance.backButton(1, "MainMenuPage");
            }

            // CUSTOM MAP EDITOR
            if (PageList[4].activeInHierarchy)
            {
                ControllerSingleton.instance.backButton(1, "MainMenuPage");
            }
        }
    }

    public void LoadNextScene(int i)
    {
        SceneManager.LoadScene(i);
    }

    public void LoadNextSceneByName(string name)
    {
        if (customMapSelected || PageList[2].GetComponent<LevelSelectHandler>().GetButtonIndex() != levels.transform.childCount - 1 || PlayerPrefs.GetString("GAMEMODE") == "Lucky Bomb")
        {
            AudioSingleton.instance.PlayButtonClick();
            AudioSingleton.instance.stopSound(AudioSingleton.SOUNDS.SOUND_MAINMENU);
            AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_GAMEPLAY, 0.025f);
            SceneManager.LoadScene(name);
        }        
    }

    public void LoadPageByName(string name)
    {
        StartCoroutine(CountDownTimer(false, 1, name));
    }

    IEnumerator CountDownTimer(bool status, float delayTime, string name)
    {
        yield return new WaitForSeconds(delayTime);

        if (!status)
        {
            foreach (GameObject GO in PageList)
            {
                if (name == GO.name)
                {
                    GO.SetActive(true);
                }
                else
                {
                    GO.SetActive(false);
                }
            }

            status = true;
        }
    }

    public void LoadPageByNameAndClearMap(string name)
    {
        GameObject tiles = GameObject.Find("Tiles");
        foreach (GameObject GO in PageList)
        {
            if (name == GO.name)
            {
                GO.SetActive(true);
                CustomMapEditor.instance.isMapPrompt = false;

                for (int i = 0; i < tiles.transform.childCount; i++)
                {
                    Destroy(tiles.transform.GetChild(i).gameObject);
                }
            }
            else
            {
                GO.SetActive(false);
            }
        }
    }

    public void LoadPageByIndex(int index)
    {
        for (int i = 0; i < PageList.Length; i++)
        {
            if (index == i)
            {
                PageList[i].SetActive(true);
            }
            else
            {
                PageList[i].SetActive(false);
            }
        }
    }

    public void QuitGame()
    {
#if UNITY_STANDALONE
        Application.Quit();
#endif

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    public void setGameMode(string gameModeName)
    {
        PlayerPrefs.SetString("GAMEMODE", gameModeName);

        for (int i = 0; i < gameModes.transform.childCount; i++)
        {
            if (PlayerPrefs.GetString("GAMEMODE") == gameModes.transform.GetChild(i).name)
            {
                gameModes.transform.GetChild(i).GetChild(0).GetComponent<Text>().color = Color.red;
                //Debug.Log("GAMEMODE IS " + gameModes.transform.GetChild(i).name);
            }

            else
            {
                gameModes.transform.GetChild(i).GetChild(0).GetComponent<Text>().color = Color.white;
            }
        }
    }
}
