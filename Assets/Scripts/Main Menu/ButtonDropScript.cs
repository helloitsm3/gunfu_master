﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class ButtonDropScript : MonoBehaviour, IDropHandler {

    public static ButtonDropScript instance;

    private Sprite charSprite;

    void Awake()
    {
        instance = this;
    }

    public GameObject itemDropped
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }

            return null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        CharSelectorScript.ItemBeingDragged.transform.SetParent(transform);
        charSprite = transform.GetComponent<Image>().sprite;

        SetCharPreview();
    }

    public void SetCharPreview()
    {
        foreach (Image img in UIManagerScript.instance.CharPreview)
        {
            if (charSprite != null)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).name == img.name)
                    {
                        img.sprite = charSprite;
                    }
                }
            }
        }
    }
}
