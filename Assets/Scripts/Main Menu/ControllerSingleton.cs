﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class ControllerSingleton : MonoBehaviour 
{
    public static ControllerSingleton instance;
    SingletonController sc;

    public enum CONTROLLER_DPAD
    {
        INPUT_NONE,
        INPUT_UP = 1,
        INPUT_DOWN = -1,
        INPUT_LEFT = -1,
        INPUT_RIGHT = 1,
    };

    List<int> DPadX;
    List<int> DPadY;
    List<int> controllerDPadSelect;
    List<bool> controllerDPadXSelectClicked;
    List<bool> controllerDPadYSelectClicked;

    int playerNumberPause;

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

	// Use this for initialization
	void Start () 
    {
        DPadX                           = new List<int>();
        DPadY                           = new List<int>();
        controllerDPadSelect            = new List<int>();
        controllerDPadXSelectClicked    = new List<bool>();
        controllerDPadYSelectClicked    = new List<bool>();
        playerNumberPause               = 0;

        for (int i = 0; i < Input.GetJoystickNames().Length; i++)
        {
            DPadX.Add(0);
            DPadY.Add(0);
            controllerDPadSelect.Add(0);
            controllerDPadXSelectClicked.Add(false);
            controllerDPadYSelectClicked.Add(false);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        // CHECK IF PLAYERS PAUSES GAME
        if (SceneManager.GetActiveScene().name == "Gameplay")
        {
            sc = SingletonController.instance;
            for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            {
                if (!sc.pauseController.PauseCanvas.activeInHierarchy)
                {
                    pauseGame(i + 1);
                }

                else
                {
                    if (i + 1 == playerNumberPause)
                    {
                        Debug.Log(i + 1);
                        GameObject buttons = sc.pauseController.gameObject.transform.GetChild(0).GetChild(2).gameObject;
                        pauseMenuInputDPad(i + 1, buttons);
                        pauseMenuInputButtons(i + 1, buttons);                     
                    }

                    else
                    {
                        //Debug.Log("Not correct player" + (i + 1).ToString());
                    }
                }
            }
        }
	}

    public void mainMenuInputDPad(int playerNumber, GameObject buttonsGroup)
    {
        for (int i = 0; i < buttonsGroup.transform.childCount; i++)
        {
            DPadY[playerNumber - 1] = (int)Input.GetAxis("Player " + playerNumber.ToString() + " DPadY");
            //Debug.Log(DPad);
            //Debug.Log(currentControllerDPadSelect);

            // RESET CLICK
            if (controllerDPadYSelectClicked[playerNumber - 1])
            {
                if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    controllerDPadYSelectClicked[playerNumber - 1] = false;
                }
            }

            // CLICK TRIGGERED
            else
            {
                if (DPadY[playerNumber - 1] != (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_DOWN)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != buttonsGroup.transform.childCount - 1)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                            AudioSingleton.instance.PlayButtonClick();
                        }
                    }

                    else if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_UP)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != 0)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                            AudioSingleton.instance.PlayButtonClick();
                        }
                    }

                    controllerDPadYSelectClicked[playerNumber - 1] = true;
                }
            }

            // CHANGE BUTTON CURRENTLY HOVERED
            if (controllerDPadSelect[playerNumber - 1] == i)
            {
                buttonsGroup.transform.GetChild(i).GetComponent<Animator>().Play("Highlighted");
            }

            else
            {
                buttonsGroup.transform.GetChild(i).GetComponent<Animator>().Play("Pressed");
            }
        }
    }

    public void mainMenuInputButtons(int playerNumber, GameObject buttonsGroup)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " A"))
        {
            buttonsGroup.transform.GetChild(controllerDPadSelect[playerNumber - 1]).gameObject.GetComponent<Button>().onClick.Invoke();
        }
    }

    public int characterSelectInputDPad(int playerNumber, int keyCount)
    {
        DPadX[playerNumber - 1] = (int)Input.GetAxis("Player " + playerNumber.ToString() + " DPadX");
        //Debug.Log(DPad);

        // RESET CLICK
        if (controllerDPadXSelectClicked[playerNumber - 1])
        {
            if (DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_NONE)
            {
                controllerDPadXSelectClicked[playerNumber - 1] = false;
                //Debug.Log("NO INPUT");
            }
        }

        // CLICK TRIGGERED
        else
        {
            if (DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_LEFT || DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_RIGHT)
            {
                controllerDPadXSelectClicked[playerNumber - 1] = true;
                keyCount += DPadX[playerNumber - 1];
                AudioSingleton.instance.PlayButtonClick();
                //Debug.Log("INPUT");
            }
        }

        return keyCount;
    }

    public void levelSelectInputDPad(int playerNumber, GameObject levels, LevelSelectHandler levelSelectPage)
    {
        for (int i = 0; i < levels.transform.childCount; i++)
        {
            DPadY[playerNumber - 1] = (int)Input.GetAxis("Player " + playerNumber.ToString() + " DPadY");
            //Debug.Log(DPad);
            //Debug.Log(currentControllerDPadSelect);
            controllerDPadSelect[playerNumber - 1] = levelSelectPage.GetButtonIndex();

            // RESET CLICK
            if (controllerDPadYSelectClicked[playerNumber - 1])
            {
                if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    controllerDPadYSelectClicked[playerNumber - 1] = false;
                }
            }

            // CLICK TRIGGERED
            else
            {
                if (DPadY[playerNumber - 1] != (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_DOWN)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != levels.transform.childCount - 1)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                        }
                    }

                    else if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_UP)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != 0)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                        }
                    }

                    controllerDPadYSelectClicked[playerNumber - 1] = true;

                    // CHANGE HIGHLIGHTED BOX
                    levels.transform.GetChild(controllerDPadSelect[playerNumber - 1]).GetComponent<Button>().Select();
                    // CHANGE CURRENT LEVEL SELECTED
                    levels.transform.GetChild(controllerDPadSelect[playerNumber - 1]).gameObject.GetComponent<Button>().onClick.Invoke();
                }
            }
        }
    }

    public void levelSelectInputButtons(int playerNumber, GameObject levels)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " A"))
        {
            // PLAYER HAVE TO MANUALLY CHOOSE MAP IF THEY SELECT CUSTOM
            if (controllerDPadSelect[playerNumber - 1] != levels.transform.childCount - 1)
            {
                UIManagerScript.instance.LoadNextSceneByName("Gameplay");
                controllerDPadSelect[playerNumber - 1] = 0;
            }
        }
    }

    public void gameModeSelectInputDPad(int playerNumber, GameObject gameModes)
    {
        for (int i = 0; i < gameModes.transform.childCount; i++)
        {
            DPadX[playerNumber - 1] = (int)Input.GetAxis("Player " + playerNumber.ToString() + " DPadX");
            //Debug.Log(DPad);
            //Debug.Log(currentControllerDPadSelect);

            // RESET CLICK
            if (controllerDPadXSelectClicked[playerNumber - 1])
            {
                if (DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    controllerDPadXSelectClicked[playerNumber - 1] = false;
                }
            }

            // CLICK TRIGGERED
            else
            {
                if (DPadX[playerNumber - 1] != (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    if (DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_LEFT)
                    {
                        gameModes.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.Invoke();
                    }

                    else if (DPadX[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_RIGHT)
                    {
                        gameModes.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.Invoke();
                    }

                    controllerDPadXSelectClicked[playerNumber - 1] = true;
                }
            }
        }
    }

    public void pauseMenuInputDPad(int playerNumber, GameObject buttons)
    {
        for (int i = 0; i < buttons.transform.childCount; i++)
        {
            DPadY[playerNumber - 1] = (int)Input.GetAxis("Player " + playerNumber.ToString() + " DPadY");
            //Debug.Log(DPad);
            //Debug.Log(currentControllerDPadSelect);

            // RESET CLICK
            if (controllerDPadYSelectClicked[playerNumber - 1])
            {
                if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    controllerDPadYSelectClicked[playerNumber - 1] = false;
                }
            }

            // CLICK TRIGGERED
            else
            {
                if (DPadY[playerNumber - 1] != (int)CONTROLLER_DPAD.INPUT_NONE)
                {
                    if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_DOWN)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != buttons.transform.childCount - 1)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                            AudioSingleton.instance.PlayButtonClick();
                        }
                    }

                    else if (DPadY[playerNumber - 1] == (int)CONTROLLER_DPAD.INPUT_UP)
                    {
                        if (controllerDPadSelect[playerNumber - 1] != 0)
                        {
                            controllerDPadSelect[playerNumber - 1] -= DPadY[playerNumber - 1];
                            AudioSingleton.instance.PlayButtonClick();
                        }
                    }

                    controllerDPadYSelectClicked[playerNumber - 1] = true;
                }
            }

            // CHANGE BUTTON CURRENTLY HOVERED
            if (controllerDPadSelect[playerNumber - 1] == i)
            {
                buttons.transform.GetChild(i).GetComponent<Animator>().Play("Highlighted");
            }

            else
            {
                buttons.transform.GetChild(i).GetComponent<Animator>().Play("Pressed");
            }
        }
    }

    public void pauseMenuInputButtons(int playerNumber, GameObject buttons)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " A"))
        {
            buttons.transform.GetChild(controllerDPadSelect[playerNumber - 1]).gameObject.GetComponent<Button>().onClick.Invoke();
        }
    }

    public void comfirmButton(int playerNumber, string pageName)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " A"))
        {
            UIManagerScript.instance.LoadPageByName(pageName);
            AudioSingleton.instance.PlayButtonClick();
            //Debug.Log("Confirm");
        }
    }

    public void backButton(int playerNumber, string pageName)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " B"))
        {
            if (SceneManager.GetActiveScene().name == "Main_Menu")
            {
                UIManagerScript.instance.LoadPageByName(pageName);
                AudioSingleton.instance.PlayButtonClick();
            }

            else if (SceneManager.GetActiveScene().name == "Gameplay")
            {
                controllerDPadSelect[playerNumber - 1] = 0;
                sc.gameController.switchToMainMenu();
            }
        }
    }

    void pauseGame(int playerNumber)
    {
        if (Input.GetButtonDown("Player " + playerNumber.ToString() + " Start"))
        {
            //Debug.Log("PAUSE");
            playerNumberPause = playerNumber;
            sc.pauseController.PauseGame();
        }
    }
}
