﻿using UnityEngine;
using System.Collections;

public class MeleeController : MonoBehaviour 
{
    float knockBack;
    float weaponRange;
    float increaseUltiAmount;

    SingletonController sc;
    GameObject player;
    GameObject otherPlayer;

	// Use this for initialization
	void Start () 
    {
        knockBack           = 12.0f;
        weaponRange         = 2.5f;
        increaseUltiAmount  = 0.1f;
        player              = transform.parent.parent.gameObject;
        sc                  = SingletonController.instance;

        // SET OTHER PLAYER
        if (player.tag == "Player1")
        {
            otherPlayer = sc.playersList[1];
        }

        else if (player.tag == "Player2")
        {
            otherPlayer = sc.playersList[0];
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        checkOtherPlayerInRange();
	}

    void checkOtherPlayerInRange()
    {
        float distance = (player.transform.position - otherPlayer.transform.position).magnitude;
        Vector3 direction = (player.transform.position - otherPlayer.transform.position).normalized;

        // ADD FORCE TO ENEMY WITHIN MELEE RANGE
        if (distance <= weaponRange)
        {
            // CHECK DIRECTION
            if (player.transform.localScale.x > 0.0f && direction.x < 0.0f || player.transform.localScale.x < 0.0f && direction.x > 0.0f)
            {
                // CHECK WHETHER PLAYER HAS INVULNERABILITY
                if (!otherPlayer.GetComponent<PlayerController>().invulnerable)
                {
                    Rigidbody2D rb = otherPlayer.GetComponent<Rigidbody2D>();

                    // SET FORCE DIRECTION
                    rb.velocity = Vector2.zero;
                    if (direction.x < 0.0f)
                    {
                        rb.velocity = new Vector2(knockBack, Mathf.Abs(knockBack));
                    }

                    else
                    {
                        rb.velocity = new Vector2(-knockBack, Mathf.Abs(knockBack));
                    }

                    // INCREASE ULTIMATE BAR
                    PlayerController playerController = player.GetComponent<PlayerController>();
                    if (!playerController.ultimate.activeInHierarchy)
                    {
                        playerController.ultibar.fillAmount += increaseUltiAmount;
                    }

                    // TOGGLE MELEE OFF
                    gameObject.SetActive(false);

                    // INCREASE PLAYER HITCOUNT
                    otherPlayer.GetComponent<PlayerController>().increaseHitCount = true;

                    // SET PLAYER GOT KNOCKBACK
                    otherPlayer.GetComponent<PlayerController>().knockedBack = true;
                }
            }
        }
    }
}
