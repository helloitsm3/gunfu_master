﻿using UnityEngine;
using System.Collections;

public class SnowBallController : MonoBehaviour 
{
    [HideInInspector]
    public float knockBack;
    [HideInInspector]
    public float moveSpeed;
    [HideInInspector]
    public bool remove;
    [HideInInspector]
    public Vector3 moveDirection;
    
	// Use this for initialization
	void Start () 
    {
        remove = false;
	}

    // SNOWBALLS GO OUT OF SCREEN
    void OnBecameInvisible()
    {
        remove = true;
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (SingletonController.instance.resumeGame)
        {
            movement();
        }
	}

    void movement()
    {
        transform.position += moveDirection * Time.deltaTime * moveSpeed;
    }

    void OnTriggerEnter2D(Collider2D collide)
    {
        // SNOWBALL HIT A PLAYER
        if (collide.tag == "Player1" || collide.tag == "Player2")
        {
            // CHECK WHETHER PLAYER HAS INVULNERABILITY
            if (!collide.GetComponent<PlayerController>().invulnerable)
            {
                Rigidbody2D rb = collide.GetComponent<Rigidbody2D>();

                // CHECK WHICH DIRECTION SNOWBALL HIT PLAYER TO APPLY KNOCKBACK DIRECTION
                Vector3 directionHitFrom = (collide.transform.position - transform.position).normalized;
                if (directionHitFrom.x > 0.0f)
                {
                    //Debug.Log("Left");
                    rb.velocity = new Vector2(knockBack, Mathf.Abs(knockBack));
                }

                else if (directionHitFrom.x < 0.0f)
                {
                    //Debug.Log("Right");
                    rb.velocity = new Vector2(-knockBack, Mathf.Abs(knockBack));
                }

                collide.GetComponent<PlayerController>().knockedBack = true;
            }

            remove = true;
        }
    }
}
