﻿using UnityEngine;
using System.Collections;

public class BombController : MonoBehaviour 
{
    public enum REMOVE_BOMB
    {
        BOMB_GOAL,
        BOMB_OUT,
        BOMB_EXPLODE,
    }
    
    [HideInInspector]
    public REMOVE_BOMB removeBomb;
    [HideInInspector]
    public bool remove;
    [HideInInspector]
    public float knockBack;
    [HideInInspector]
    public bool timeBombSoundOnce;
    [HideInInspector]
    public bool textSoundOnce;

    SingletonController sc;
    float explodeTiming;
    float explodeTimer;
    float flickerTimer;
    float flickerDuration;
    float outTimer;
    float outDuration;
    Vector2 pausedVelocity;
    

	// Use this for initialization
	void Start () 
    {
        sc                  = SingletonController.instance;
        remove              = false;
        knockBack           = 16.0f;
        explodeTiming       = 0.0f;
        explodeTimer        = 0.0f;
        flickerTimer        = 0.0f;
        flickerDuration     = 0.25f;
        outTimer            = 0.0f;
        outDuration         = 1.0f;
        pausedVelocity      = new Vector2(0.0f, 0.0f);
        timeBombSoundOnce   = false;
        textSoundOnce       = false;

        randomizeExplodeTiming();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (sc.resumeGame)
        {
            // BOMB CANNOT BE SEEN
            if (!gameObject.GetComponent<SpriteRenderer>().isVisible)
            {
                bombGoesOut();
            }

            else
            {
                timeBomb();
            }

            // UNPAUSE BOMB
            if (gameObject.GetComponent<Rigidbody2D>().IsSleeping())
            {
                gameObject.GetComponent<Rigidbody2D>().WakeUp();
                gameObject.GetComponent<Rigidbody2D>().velocity = pausedVelocity;
            }
        }

        // PAUSE GRENADE
        else
        {
            if (gameObject.GetComponent<Rigidbody2D>().IsAwake())
            {
                pausedVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
                gameObject.GetComponent<Rigidbody2D>().Sleep();
            }
        }
	}
    
    void bombGoesOut()
    {
        outTimer += Time.deltaTime;
        if (outTimer >= outDuration)
        {
            remove = true;
            removeBomb = REMOVE_BOMB.BOMB_OUT;
            outTimer = 0.0f;
            //Debug.Log("Out");
        }
    }

    void timeBomb()
    {
        explodeTimer += Time.deltaTime;
        outTimer = 0.0f;
     
        // BOMB EXPLODES
        if (explodeTimer >= explodeTiming)
        {
            remove = true;
            removeBomb = REMOVE_BOMB.BOMB_EXPLODE;
            sc.objectSpawner.spawnExplosions(gameObject.transform);
            //Debug.Log("BOOM");
        }

        else
        {
            // BOMB STARTS TO FLICKER
            if (explodeTimer >= (explodeTiming / 2))
            {
                flickerTimer += Time.deltaTime;
                if (flickerTimer >= flickerDuration)
                {
                    flickerTimer = 0.0f;
                    if (gameObject.GetComponent<SpriteRenderer>().color == Color.white)
                    {
                        gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                    }

                    else
                    {
                        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                    }                    
                }

                if (!timeBombSoundOnce)
                {
                    AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_TIMEBOMB, 0.1f);
                    timeBombSoundOnce = true;
                }
            }
        }
    }

    public void randomizeExplodeTiming()
    {
        // RANDOMIZE RANGE FROM 10 - 15
        explodeTiming = Random.Range(10, 16);
        // RESET TIMER
        explodeTimer = 0.0f;
        // RESET COLOR
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
