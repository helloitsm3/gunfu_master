﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingletonController : MonoBehaviour 
{
    public static SingletonController instance;
    public GameController gameController;
    public ObjectSpawner objectSpawner;
    public PauseController pauseController;
    public List<GameObject> characters;

    [HideInInspector]
    public float screenHeight;
    [HideInInspector]
    public float screenWidth;
    [HideInInspector]
    public List<GameObject> playersList;
    [HideInInspector]
    public bool resumeGame;
    [HideInInspector]
    public List<int> characterSelect;

    int numofPlayers;
    float initialSpawnX;

    void Awake()
    {
        instance        = this;
        gameController  = gameController.gameObject.GetComponent<GameController>();
        objectSpawner   = objectSpawner.gameObject.GetComponent<ObjectSpawner>();
        pauseController = pauseController.gameObject.GetComponent<PauseController>();
        characterSelect = new List<int>();
        playersList     = new List<GameObject>();
        numofPlayers    = 2;
        initialSpawnX   = 4.0f;
        
        for (int i = 0; i < numofPlayers; i++)
        {
            characterSelect.Add(PlayerPrefs.GetInt("Player " + (i + 1).ToString() + " Index"));
        }

        for (int i = 0; i < characterSelect.Count; i++)
        {
            GameObject selectedCharacter = characters[characterSelect[i]];
            GameObject characterCreated = (GameObject)Instantiate(selectedCharacter, selectedCharacter.transform.position, selectedCharacter.transform.rotation);
            characterCreated.name = "Player" + (i + 1).ToString();
            characterCreated.tag = characterCreated.name;
            characterCreated.layer = LayerMask.NameToLayer("Players");
            characterCreated.GetComponent<PlayerController>().characterUltimate = (PlayerController.ultimateList)characterSelect[i];
            characterCreated.transform.position = new Vector3(-initialSpawnX, 0.0f, 0.0f);
            
            if (i == 1)
            {
                Vector3 tempPos = characterCreated.transform.position;
                characterCreated.transform.position = new Vector3(initialSpawnX, tempPos.y, tempPos.z);
                Vector3 tempScale = characterCreated.transform.localScale;
                characterCreated.transform.localScale = new Vector3(tempScale.x * -1.0f, tempScale.y, tempScale.z);
            }

            characterCreated.GetComponent<PlayerController>().initialPos = characterCreated.transform.position;
            playersList.Add(characterCreated);
        }
    }

	// Use this for initialization
	void Start () 
    {
        screenHeight            = Camera.main.orthographicSize;
        screenWidth             = Camera.main.orthographicSize * Camera.main.aspect;
        resumeGame              = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
	}
}
