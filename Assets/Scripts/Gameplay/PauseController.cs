﻿using UnityEngine;
using System.Collections;

public class PauseController : MonoBehaviour 
{
    public GameObject PauseCanvas;
    SingletonController sc;

	// Use this for initialization
	void Start () 
    {
        PauseCanvas.SetActive(false);
        sc = SingletonController.instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKey(KeyCode.P))
            PauseGame();
	}

    public void PauseGame()
    {
        //Time.timeScale = 0;
        sc.resumeGame = false;
        PauseCanvas.SetActive(true);

        // PAUSE PLAYERS' RIGIDBODIES
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            sc.playersList[i].GetComponent<PlayerController>().pausedVelocity = sc.playersList[i].GetComponent<Rigidbody2D>().velocity; 
            sc.playersList[i].GetComponent<Rigidbody2D>().Sleep();
        }

        AudioSingleton.instance.PlayButtonClick();
    }

    public void ResumeGame()
    {
        SingletonController.instance.resumeGame = true;
        PauseCanvas.SetActive(false);

        // UNPAUSE PLAYERS' RIGIDBODIES
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            sc.playersList[i].GetComponent<Rigidbody2D>().WakeUp();
            sc.playersList[i].GetComponent<Rigidbody2D>().velocity = sc.playersList[i].GetComponent<PlayerController>().pausedVelocity;
        }

        AudioSingleton.instance.PlayButtonClick();
    }
}
