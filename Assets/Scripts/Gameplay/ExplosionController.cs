﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExplosionController : MonoBehaviour 
{
    [HideInInspector]
    public bool remove;
    
    PlayerController playerController;
    SingletonController sc;
    GameObject player;
    
    float explosionRadius;
    float increaseUltiAmount;

    void Awake()
    {
        explosionRadius     = 3.5f;
        sc                  = SingletonController.instance;
    }

	// Use this for initialization
	void Start () 
    {
        remove                  = false;
        increaseUltiAmount      = 0.2f;
	}

	// Update is called once per frame
	void Update () 
    {
	}

    public void checkExplosionRange(float knockBack)
    {
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            // CHECK KNIGHT'S ULTIMATE
            if (sc.playersList[i].GetComponent<PlayerController>().characterUltimate == PlayerController.ultimateList.ULTIMATE_IMMUNITY && sc.playersList[i].GetComponent<PlayerController>().ultimate.activeInHierarchy)
            {
                break;
            }

            // ADD FORCE TO NEARBY PLAYERS WITHIN GRENADE RANGE
            float distance = (transform.position - sc.playersList[i].transform.position).magnitude;
            if (distance <= explosionRadius)
            {
                // CHECK WHETHER PLAYER HAS INVULNERABILITY
                if (!sc.playersList[i].GetComponent<PlayerController>().invulnerable)
                {
                    Rigidbody2D rb = sc.playersList[i].gameObject.GetComponent<Rigidbody2D>();
                    Vector3 direction = (transform.position - sc.playersList[i].transform.position).normalized;
                    //Debug.Log(sc.playersList[i].name + " got knocked back");

                    // SET FORCE DIRECTION
                    rb.velocity = Vector2.zero;
                    if (direction.x > 0.0f)
                    {
                        //Debug.Log("Player" + (i + 1).ToString() + " goes left");
                        rb.velocity = new Vector2(-knockBack, Mathf.Abs(knockBack));
                    }

                    else
                    {
                        //Debug.Log("Player" + (i + 1).ToString() + " goes right");
                        rb.velocity = new Vector2(knockBack, Mathf.Abs(knockBack));
                    }

                    // INCREASE ULTIMATE BAR
                    if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
                    {
                        if (sc.playersList[i].tag == "Player2" && transform.tag != sc.playersList[i].tag)
                        {
                            player = sc.playersList[i - 1];
                            playerController = player.GetComponent<PlayerController>();
                            if (!playerController.ultimate.activeInHierarchy)
                            {
                                playerController.ultibar.fillAmount += increaseUltiAmount;
                            }
                        }

                        else if (sc.playersList[i].tag == "Player1" && transform.tag != sc.playersList[i].tag)
                        {
                            player = sc.playersList[i + 1];
                            playerController = player.GetComponent<PlayerController>();
                            if (!playerController.ultimate.activeInHierarchy)
                            {
                                playerController.ultibar.fillAmount += increaseUltiAmount;
                            }
                        }
                    }

                    sc.playersList[i].GetComponent<PlayerController>().knockedBack = true;
                }

                // PLAYER UNAFFECTED DUE TO INVULNERABILITY
                else
                {
                    sc.playersList[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                }
            }
        }
    }

    public void explosionCompleted()
    {
        remove = true;
    }
}
