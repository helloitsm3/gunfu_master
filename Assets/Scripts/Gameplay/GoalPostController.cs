﻿using UnityEngine;
using System.Collections;

public class GoalPostController : MonoBehaviour 
{
    SingletonController sc;

	// Use this for initialization
	void Start () 
    {
        sc = SingletonController.instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void OnCollisionEnter2D(Collision2D collide)
    {
        if (collide.gameObject.name == "Bomb")
        {
            for (int i = 0; i < sc.playersList.Count; i++)
            {
                if (gameObject.tag == sc.playersList[i].name)
                {
                    sc.playersList[i].GetComponent<PlayerController>().updateLives();
                    collide.gameObject.GetComponent<BombController>().remove = true;
                    collide.gameObject.GetComponent<BombController>().removeBomb = BombController.REMOVE_BOMB.BOMB_GOAL;
                }
            }
        }
    }
}
