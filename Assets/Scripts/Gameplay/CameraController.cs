﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    SingletonController sc;
    new Camera camera;
    GameObject player1;
    GameObject player2;

    float zoomSpeed;
    float maxOrthographicSize;
    float minOrthographicSize;
    float mapXTiles;
    float mapYTiles;
    float boundingBoxPadding;

    // Use this for initialization
    void Start()
    {
        sc = SingletonController.instance;
        camera = gameObject.GetComponent<Camera>();
        player1 = sc.playersList[0];
        player2 = sc.playersList[1];

        zoomSpeed = 1.5f;
        maxOrthographicSize = camera.orthographicSize;
        minOrthographicSize = 7.0f;
        mapXTiles = PlayerPrefs.GetInt("MAP_WIDTH_SIZE");
        mapYTiles = PlayerPrefs.GetInt("MAP_HEIGHT_SIZE");
        boundingBoxPadding = 5.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (sc.resumeGame)
        {
            if (!player1.GetComponent<PlayerController>().respawn && !player2.GetComponent<PlayerController>().respawn)
            {
                updateGameCamera();
            }

            else
            {
                setCameraToPlayerOnScreen();
            }
        }

        cameraLimits();
    }

    Rect CalculateTargetsBoundingBox()
    {
        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        for (int i = 0; i < sc.playersList.Count; i++)
        {
            Vector3 position = sc.playersList[i].transform.position;

            minX = Mathf.Min(minX, position.x);
            minY = Mathf.Min(minY, position.y);
            maxX = Mathf.Max(maxX, position.x);
            maxY = Mathf.Max(maxY, position.y);
        }

        // TAKE BOMB INTO CONSIDERATION OF CAMERA BOUNDING BOX
        if (PlayerPrefs.GetString("GAMEMODE") == "Lucky Bomb")
        {
            Vector3 position = sc.objectSpawner.bombObj.transform.position;

            minX = Mathf.Min(minX, position.x);
            minY = Mathf.Min(minY, position.y);
            maxX = Mathf.Max(maxX, position.x);
            maxY = Mathf.Max(maxY, position.y);
        }

        return Rect.MinMaxRect(minX - boundingBoxPadding, maxY + boundingBoxPadding, maxX + boundingBoxPadding, minY - boundingBoxPadding);
    }

    Vector3 CalculateCameraPosition(Rect boundingBox)
    {
        return new Vector3(boundingBox.center.x, boundingBox.center.y, camera.transform.position.z);
    }

    float CalculateOrthographicSize(Rect boundingBox)
    {
        float orthographicSize = camera.orthographicSize;
        Vector3 topRight = new Vector3(boundingBox.x + boundingBox.width, boundingBox.y, 0f);
        Vector3 topRightAsViewport = camera.WorldToViewportPoint(topRight);

        if (topRightAsViewport.x >= topRightAsViewport.y)
        {
            orthographicSize = Mathf.Abs(boundingBox.width) / camera.aspect / 2.0f;
        }

        else
        {
            orthographicSize = Mathf.Abs(boundingBox.height) / 2.0f;
        }

        return Mathf.Clamp(Mathf.Lerp(camera.orthographicSize, orthographicSize, Time.deltaTime * zoomSpeed), minOrthographicSize, maxOrthographicSize);
    }

    void setCameraToPlayerOnScreen()
    {
        // SET POSITION
        for (int i = 0; i < SingletonController.instance.playersList.Count; i++)
        {
            // 1 OR MORE PLAYER RESPAWNING
            if (!SingletonController.instance.playersList[i].GetComponent<PlayerController>().respawn)
            {
                Vector3 newPos = SingletonController.instance.playersList[i].transform.position;
                newPos = new Vector3(newPos.x, newPos.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime);
                break;
            }

            else
            {
                Vector3 newPos = new Vector3(0.0f, 0.0f, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime);
            }
        }

        // SET CAMERA SIZE
        camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, (maxOrthographicSize + minOrthographicSize) * 0.5f, Time.deltaTime);
    }

    void updateGameCamera()
    {
        Rect boundingBox = CalculateTargetsBoundingBox();
        transform.position = CalculateCameraPosition(boundingBox);
        camera.orthographicSize = CalculateOrthographicSize(boundingBox);
    }

    void cameraLimits()
    {
        // WORLD COORDINATE SIZES
        float vertExtent = camera.orthographicSize;
        float horzExtent = vertExtent * sc.screenWidth / sc.screenHeight;

        // CALCULATE POSITION LIMITS
        float minX = horzExtent - mapXTiles / 2.0f;
        float maxX = mapXTiles / 2.0f - horzExtent;
        float minY = vertExtent - mapYTiles / 2.0f;
        //float maxY = mapYTiles / 2.0f - vertExtent;

        // CLAMP POSITIONS TO WITHIN MAP SIZE
        Vector3 tempPos = transform.position;
        tempPos.x = Mathf.Clamp(tempPos.x, minX, maxX);
        tempPos.y = Mathf.Clamp(tempPos.y, minY, Mathf.Infinity);
        transform.position = tempPos;
    }
}
