﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour 
{
    public enum WeaponList
    {
        WEAPON_PISTOL = 1,
        WEAPON_RIFLE,
        WEAPON_LAUNCHER,
    };

    public enum ultimateList
    {
        ULTIMATE_BURSTSHOTS,
        ULTIMATE_DOUBLEJUMP,
        ULTIMATE_GRENADEBOOST,
        ULTIMATE_IMMUNITY,
    };
    
    public LayerMask groundLayer;
    public GameObject weapons;
    public GameObject ultimate;

    [HideInInspector]
    public bool knockedBack;
    [HideInInspector]
    public Image ultibar;
    [HideInInspector]
    public bool respawn;
    [HideInInspector]
    public float respawnDelayTimer;
    [HideInInspector]
    public bool invulnerable;
    [HideInInspector]
    public float invulnerableDurationTimer;
    [HideInInspector]
    public bool flicker;
    [HideInInspector]
    public int hitCount;
    [HideInInspector]
    public bool increaseHitCount;
    [HideInInspector]
    public bool shrinkHitCount;
    [HideInInspector]
    public int lives;
    [HideInInspector]
    public GameObject pose;
    [HideInInspector]
    public Vector2 pausedVelocity;
    [HideInInspector]
    public ultimateList characterUltimate;
    [HideInInspector]
    public bool knockBackSoundOnce;
    [HideInInspector]
    public bool respawnSoundOnce;
    [HideInInspector]
    public Vector3 initialPos;

    SingletonController sc;
    Animator anim;
    Rigidbody2D rb;
    Transform groundCheck;
    GameObject playerUI;
    GameObject lifeBar;
    
    WeaponList activeWeapon;
    KeyCode inputLeft, inputRight, inputJump, inputUltimate, inputMelee, inputShoot, inputSwapWeapons;

    float horizontal;
    float moveSpeed;
    float jumpHeight;
    float groundcheckRadius;
    float recoverVelocity;
    float ultimateDrain;
    float airResistance;

    bool grounded;
    bool jumped;
    bool move;    

    string playerNumber;
    string charUltiIconNum;

    int playerNumberCharSelect;

	// Use this for initialization
	void Start () 
    {
        sc                  = SingletonController.instance;
        anim                = GetComponent<Animator>();
        rb                  = GetComponent<Rigidbody2D>();
        groundCheck         = transform.Find("GroundCheck").transform;
        activeWeapon        = WeaponList.WEAPON_PISTOL;
        
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            if (transform.tag + "UI" == sc.gameController.playerUIList[i].name)
            {
                playerUI = sc.gameController.GetComponent<GameController>().playerUIList[i];
            }

            if (transform.tag + "Pose" == sc.gameController.playerPoseList[i].name)
            {
                pose = sc.gameController.GetComponent<GameController>().playerPoseList[i].gameObject;
            }
        }
        
        ultibar                     = playerUI.transform.Find("UltiBar").GetChild(0).GetComponent<Image>();
        lifeBar                     = playerUI.transform.Find("LifeBar").gameObject;
        
        moveSpeed                   = 10.5f;
        jumpHeight                  = 25.0f;
        groundcheckRadius           = 0.5f;
        recoverVelocity             = 1.0f;
        ultimateDrain               = 0.002f;
        airResistance               = 0.2f;
        respawnDelayTimer           = 0.0f;
        invulnerableDurationTimer   = 0.0f;
        hitCount                    = 0;
        lives                       = 4;
        
        knockedBack                 = false;
        jumped                      = false;
        move                        = true;
        respawn                     = false;
        invulnerable                = false;
        flicker                     = false;
        increaseHitCount            = false;
        shrinkHitCount              = false;
        knockBackSoundOnce          = false;
        respawnSoundOnce            = false;

        // SET PLAYER NUMBER
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            if (transform.tag == sc.playersList[i].name)
            {
                playerNumber = "Player " + (i + 1).ToString() + " ";
                playerNumberCharSelect = i;
            }

            // SET ULTIMATE COLOR FOR PLAYER 2
            if (transform.tag == sc.playersList[1].name)
            {
                ultimate.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Ultimate2/Ultimate2");
            }
        }

        // SET CHARACTER ICON
        charUltiIconNum = sc.characterSelect[playerNumberCharSelect].ToString();
        playerUI.transform.Find("CharacterIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Game_UI/Char" + charUltiIconNum + "_Icon");

        // EDIT PLAYER UI BASED ON GAME MODE
        if (PlayerPrefs.GetString("GAMEMODE") == "Lucky Bomb")
        {
            for (int i = 0; i < playerUI.transform.childCount; i++)
            {
                if (playerUI.transform.GetChild(i).name != "CharacterIcon" && playerUI.transform.GetChild(i).name != "LifeBar")
                {
                    playerUI.transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        // SET PLAYER CONTROLS
        inputLeft           = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Left Button", "A"));
        inputRight          = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Right Button", "D"));
        inputJump           = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Jump Button", "W"));
        inputUltimate       = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Ultimate Button", "S"));
        inputMelee          = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Melee Button", "Q"));
        inputShoot          = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Shoot Button", "E"));
        inputSwapWeapons    = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString(playerNumber + "Weapon Switch Button", "E"));
	}

	// Update is called once per frame
	void Update () 
    {
        if (sc.resumeGame)
        {
            movement();
            jump();
            knockBackRecover();
            
            if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
            {
                attack();
                swapWeapons();
                activateUltimate();
            }
        }
	}

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundcheckRadius, groundLayer);
        
        if (grounded)
        {
            move = true;
            jumped = false;
        }
    }

    void LateUpdate()
    {
        playerUI.transform.Find("WeaponIcon").GetComponent<Image>().SetNativeSize();
        playerUI.transform.Find("AmmoIcon").GetComponent<Image>().SetNativeSize();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground") && !grounded)
        {
            move = false;
        }

        if (collision.gameObject.GetComponent<FallingPlatform>() != null)
        {
            collision.gameObject.GetComponent<TilesInteractHandler>().SetInteractable(collision.gameObject, true);
        }
    }

    // PLAYER GOES OUT OF SCREEN
    void OnBecameInvisible()
    {
        updateLives();     
        if (lives > 0)
        {
            respawn = true;
        }
    }

    public void updateLives()
    {
        lives--;
        if (lifeBar != null)
        {
            for (int i = 0; i < lifeBar.transform.childCount; i++)
            {
                if (i < lives)
                {
                    lifeBar.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Game_UI/Health_Active");
                }

                else
                {
                    lifeBar.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Game_UI/Health_Inactive");
                }
            }
        }
    }

    void activateUltimate()
    {
        // ACTIVATE ULTIMATE
        if (!ultimate.activeInHierarchy)
        {
            if (ultibar.fillAmount == 1.0f)
            {
                playerUI.transform.Find("CharacterIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Game_UI/Char" + charUltiIconNum + "_Icon_Ulti");

                int padUlti = (int)Input.GetAxis(playerNumber + "DPadY");

                if (Input.GetKeyDown(inputUltimate) || padUlti == -1)
                {
                    ultimate.SetActive(true);
                    AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_ULTIMATE, 0.05f);
                }
            }
        }

        // ULTIMATE IN USE
        else
        {
            ultibar.fillAmount -= ultimateDrain;
            if (ultibar.fillAmount <= 0.0f)
            {
                playerUI.transform.Find("CharacterIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Game_UI/Char" + charUltiIconNum + "_Icon");
                ultimate.SetActive(false);
            }
        }
    }

    void knockBackRecover()
    {
        if (knockedBack)
        {
            // RECOVER PLAYER FROM KNOCKBACK
            if (rb.velocity.x >= -recoverVelocity && rb.velocity.x <= recoverVelocity)
            {
                hitCount = 0;
                knockedBack = false;
                knockBackSoundOnce = false;
                //Debug.Log(gameObject.name + " Recover");
            }

            else
            {
                if (!knockBackSoundOnce)
                {
                    AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_KNOCKBACK, 0.5f);
                    knockBackSoundOnce = true;
                }
            }
        }
    }

    void swapWeapons()
    {
        playerUI.transform.Find("Ammo").GetComponent<Text>().text = weapons.transform.GetChild((int)activeWeapon).GetComponent<WeaponController>().ammo.ToString();

        // ONLY ALLOW PLAYER TO SWAP WEAPON WHEN ACTIVE WEAPON IS FALSE
        if (Input.GetKeyDown(inputSwapWeapons) || Input.GetButtonDown(playerNumber + "Y"))
        {
            if (!weapons.transform.GetChild((int)activeWeapon).gameObject.activeInHierarchy)
            {
                if (activeWeapon == WeaponList.WEAPON_PISTOL)
                {
                    activeWeapon = WeaponList.WEAPON_RIFLE;
                    playerUI.transform.Find("WeaponIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Rifle");
                    playerUI.transform.Find("AmmoIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Bullet");
                }

                else if (activeWeapon == WeaponList.WEAPON_RIFLE)
                {
                    activeWeapon = WeaponList.WEAPON_LAUNCHER;
                    playerUI.transform.Find("WeaponIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Launcher");
                    playerUI.transform.Find("AmmoIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Grenade");
                }

                else if (activeWeapon == WeaponList.WEAPON_LAUNCHER)
                {
                    activeWeapon = WeaponList.WEAPON_PISTOL;
                    playerUI.transform.Find("WeaponIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Pistol");
                    playerUI.transform.Find("AmmoIcon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Weapons/Bullet");
                }
            }
        }
    }

    void movement()
    {
        // MOVING
        if (!knockedBack)
        {
            if (move)
            {
                int padMove = (int)Input.GetAxis(playerNumber + "DPadX");

                if (Input.GetKey(inputLeft) || padMove == -1)
                {
                    horizontal = -1.0f;
                    rb.velocity = new Vector2(horizontal * moveSpeed, rb.velocity.y);
                }

                else if (Input.GetKey(inputRight) || padMove == 1)
                {
                    horizontal = 1.0f;
                    rb.velocity = new Vector2(horizontal * moveSpeed, rb.velocity.y);
                }
            }
        }

        // AIR RESISTANCE WHEN KNOCKED BACK
        else
        {
            if (rb.velocity.x > 0.0f)
            {
                rb.velocity -= new Vector2(airResistance, 0.0f);
            }

            else if (rb.velocity.x < 0.0f)
            {
                rb.velocity += new Vector2(airResistance, 0.0f);
            }
        }

        // ADJUST PLAYER DIRECTION
        if (horizontal > 0.0f)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }

        else if (horizontal < 0.0f)
        {
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }

        anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }

    void jump()
    {
        // JUMP
        if (Input.GetKeyDown(inputJump) || Input.GetButtonDown(playerNumber + "A"))
        {
            if (!jumped)
            {
                if (grounded || (!grounded && knockedBack) || (characterUltimate == ultimateList.ULTIMATE_DOUBLEJUMP && ultimate.activeInHierarchy && !knockedBack))
                {
                    rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
                    jumped = true;
                }
            }
        }   

        anim.SetFloat("VelocityY", rb.velocity.y);
        anim.SetBool("Grounded", grounded);
    }

    void attack()
    {
        // MELEE
        if (Input.GetKeyDown(inputMelee) || Input.GetButtonDown(playerNumber + "X"))
        {
            weapons.transform.Find("Melee").gameObject.SetActive(true);
            anim.SetTrigger("Melee");
            AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_MELEE, 0.1f);
        }

        // SHOOT
        GameObject weapon = weapons.transform.GetChild((int)activeWeapon).gameObject;
        WeaponController wc = weapon.GetComponent<WeaponController>();

        if (weapon.GetComponent<WeaponController>().ammo > 0 && !weapon.activeInHierarchy)
        {
            // SINGLE SHOT
            if (Input.GetKeyDown(inputShoot) || Input.GetButtonDown(playerNumber + "B"))
            {
                if (weapon.name != "Rifle")
                {
                    weapon.SetActive(true);

                    if (weapon.name == "Pistol")
                    {
                        sc.objectSpawner.fireBullet(weapon.transform, transform, weapon.name, 90.0f);

                        // GIRL'S ULTIMATE
                        if (characterUltimate == ultimateList.ULTIMATE_BURSTSHOTS && ultimate.activeInHierarchy)
                        {
                            float offsetTragectory = 5.0f;
                            sc.objectSpawner.fireBullet(weapon.transform, transform, weapon.name, 90.0f + offsetTragectory);
                            sc.objectSpawner.fireBullet(weapon.transform, transform, weapon.name, 90.0f - offsetTragectory);
                        }

                        AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_PISTOL, 0.1f);
                    }

                    else if (weapon.name == "Launcher")
                    {
                        sc.objectSpawner.fireGrenade(weapon.transform, transform);
                        AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_GRENADE, 0.5f);
                    }

                    anim.SetTrigger("Shoot");
                    wc.ammo--;
                }
            }

            // AUTOMATIC
            else if (Input.GetKey(inputShoot) || Input.GetButton(playerNumber + "B"))
            {
                if (weapon.name == "Rifle")
                {
                    weapon.SetActive(true);
                 
                    if (wc.fireRateTimer >= wc.fireRate)
                    {
                        wc.fireRateTimer = 0.0f;
                        sc.objectSpawner.fireBullet(weapon.transform, transform, weapon.name, 90.0f);
                        anim.SetTrigger("Shoot");
                        wc.ammo--;
                        AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_RIFLE, 0.1f);
                    }

                    else
                    {
                        wc.fireRateTimer += Time.deltaTime;
                    }
                }
            }
        }
    }

     // TOGGLE CURRENT WEAPON OFF
    public void toggleWeaponOff()
    {
        weapons.transform.Find("Melee").gameObject.SetActive(false);
        weapons.transform.GetChild((int)activeWeapon).gameObject.SetActive(false);
        weapons.transform.GetChild((int)activeWeapon).gameObject.GetComponent<WeaponController>().fireRateTimer = weapons.transform.GetChild((int)activeWeapon).gameObject.GetComponent<WeaponController>().fireRate;
    }

    // RESET TRANSFORM BACK TO ORIGINAL
    public void resetOriginalSpawn()
    {
        transform.position = initialPos;
        
        // SET PLAYER FACING
        if (gameObject.tag == sc.playersList[0].name)
        {
            horizontal = 1.0f;
        }

        else 
        {
            horizontal = -1.0f;
        }
    }
}
