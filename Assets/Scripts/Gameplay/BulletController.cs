﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BulletController : MonoBehaviour 
{
    [HideInInspector]
    public float moveSpeed;
    [HideInInspector]
    public float knockBack;
    [HideInInspector]
    public bool remove;

    SingletonController sc;
    GameObject player;

    float increaseUltiAmount;

	// Use this for initialization
	void Start () 
    {
        remove              = false;
        increaseUltiAmount  = 0.1f;
        sc                  = SingletonController.instance;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (SingletonController.instance.resumeGame)
        {
            movement();
        }
	}

    // BULLETS GO OUT OF SCREEN
    void OnBecameInvisible()
    {
        remove = true;
    }

    void movement()
    {
        Vector3 bulletVelocity = new Vector3(0, moveSpeed * Time.deltaTime, 0);
        transform.position -= transform.rotation * bulletVelocity;
    }

    // BULLETS HIT OTHER PLAYER
    void OnTriggerEnter2D(Collider2D gameobjectCollided)
    {
        // PREVENT PLAYER'S BULLET FROM HITTING THEMSELVES
        if (transform.tag == "Player1" && gameobjectCollided.tag == "Player2" || transform.tag == "Player2" && gameobjectCollided.tag == "Player1")
        {
            // CHECK KNIGHT'S ULTIMATE
            if (gameobjectCollided.GetComponent<PlayerController>().characterUltimate == PlayerController.ultimateList.ULTIMATE_IMMUNITY && gameobjectCollided.GetComponent<PlayerController>().ultimate.activeInHierarchy)
            {
                remove = true;
                return;
            }

            // CHECK WHETHER PLAYER HAS INVULNERABILITY
            if (!gameobjectCollided.GetComponent<PlayerController>().invulnerable)
            {
                Rigidbody2D rb = gameobjectCollided.GetComponent<Rigidbody2D>();

                // SET FORCE DIRECTION
                rb.velocity = Vector2.zero;
                if (moveSpeed > 0.0f)
                {
                    rb.velocity = new Vector2(knockBack, Mathf.Abs(knockBack));
                }

                else
                {
                    rb.velocity = new Vector2(-knockBack, Mathf.Abs(knockBack));
                }

                // INCREASE ULTIMATE BAR
                for (int i = 0; i < sc.playersList.Count; i++)
                {
                    if (transform.tag == sc.playersList[i].name)
                    {
                        player = sc.playersList[i];
                    }
                }

                PlayerController playerController = player.GetComponent<PlayerController>();
                if (!playerController.ultimate.activeInHierarchy)
                {
                    playerController.ultibar.fillAmount += increaseUltiAmount;
                    gameobjectCollided.GetComponent<PlayerController>().increaseHitCount = true;
                }

                gameobjectCollided.GetComponent<PlayerController>().knockedBack = true;
            }

            remove = true;
        }

        // HIT BARRELS
        else if (gameobjectCollided.GetComponent<ExplodeBarrelTiles>() != null)
        {
            gameobjectCollided.GetComponent<ExplodeBarrelTiles>().damageBarrel(1);
            remove = true;
        }
    }
}
