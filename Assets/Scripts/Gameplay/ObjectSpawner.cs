﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour 
{
    enum ObjectType
    {
        OBJECT_AMMO,
        OBJECT_CRATE,
        OBJECT_EXPLOSION,
        OBJECT_SNOWBALL,
        OBJECT_BOMB,
        OBJECT_GOALPOST,
    };

    public int numOfAmmo;
    public int numOfCrates;
    public GameObject bullet;
    public GameObject grenade;
    public GameObject cratePistol;
    public GameObject crateRifle;
    public GameObject crateLauncher;
    public GameObject explosion;
    public GameObject snowball;
    public GameObject bomb;
    public GameObject goalPost;
    [HideInInspector]
    public GameObject bombObj;
    
    List<GameObject> bulletsList;
    List<GameObject> grenadesList;
    List<GameObject> cratesList;
    List<GameObject> explosionsList;
    List<GameObject> snowballsList;
    List<int> cratesIDList;
    GameObject crateObj;
    SingletonController sc;
    
    int numOfCrateTypes;
    int numOfCratesOnScreen;
    int crateIDIncrement;
    int numOfGoalPosts;
    float spawnCratesDelay;
    float spawnCratesDelayReset;

	// Use this for initialization
	void Start () 
    {
        bulletsList             = new List<GameObject>();
        grenadesList            = new List<GameObject>();
        cratesList              = new List<GameObject>();
        explosionsList          = new List<GameObject>();
        snowballsList           = new List<GameObject>();
        cratesIDList            = new List<int>();
        sc                      = SingletonController.instance;
        numOfCratesOnScreen     = 0;
        numOfCrateTypes         = 3;
        crateIDIncrement        = 0;
        numOfGoalPosts          = 2;
        spawnCratesDelay        = 2.5f;
        spawnCratesDelayReset   = spawnCratesDelay;

        // DEATH MATCH OBJECTS
        for (int i = 0; i < numOfAmmo; i++)
        {
            GameObject bulletObj = (GameObject)Instantiate(bullet, bullet.transform.position, bullet.transform.rotation);
            bulletObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_AMMO).transform;
            bulletObj.name = bullet.name;
            bulletObj.SetActive(false);
            bulletsList.Add(bulletObj);
        }

        for (int i = 0; i < numOfAmmo; i++)
        {
            GameObject grenadeObj = (GameObject)Instantiate(grenade, grenade.transform.position, grenade.transform.rotation);
            grenadeObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_AMMO).transform;
            grenadeObj.name = grenade.name;
            grenadeObj.SetActive(false);
            grenadesList.Add(grenadeObj);
        }

        for (int i = 0; i < numOfCrateTypes; i++)
        {
            for (int x = 0; x < numOfCrates; x++)
            {
                if (i == 0)
                {
                    crateObj = (GameObject)Instantiate(cratePistol, cratePistol.transform.position, cratePistol.transform.rotation);
                    crateObj.name = cratePistol.name;
                }

                else if (i == 1)
                {
                    crateObj = (GameObject)Instantiate(crateRifle, crateRifle.transform.position, crateRifle.transform.rotation);
                    crateObj.name = crateRifle.name;
                }

                else if (i == 2)
                {
                    crateObj = (GameObject)Instantiate(crateLauncher, crateLauncher.transform.position, crateLauncher.transform.rotation);
                    crateObj.name = crateLauncher.name;
                }

                crateObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_CRATE).transform;
                crateObj.GetComponent<CrateController>().id = crateIDIncrement;
                crateObj.SetActive(false);
                cratesList.Add(crateObj);
                crateIDIncrement++;
            }
        }

        for (int i = 0; i < numOfAmmo; i++)
        {
            GameObject explosionObj = (GameObject)Instantiate(explosion, explosion.transform.position, explosion.transform.rotation);
            explosionObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_EXPLOSION).transform;
            explosionObj.name = explosion.name;
            explosionObj.SetActive(false);
            explosionsList.Add(explosionObj);
        }

        for (int i = 0; i < numOfAmmo; i++)
        {
            GameObject snowballObj = (GameObject)Instantiate(snowball, snowball.transform.position, snowball.transform.rotation);
            snowballObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_SNOWBALL).transform;
            snowballObj.name = snowball.name;
            snowballObj.SetActive(false);
            snowballsList.Add(snowballObj);
        }

        // LUCKY BOMB OBJECTS
        bombObj = (GameObject)Instantiate(bomb, bomb.transform.position, bomb.transform.rotation);
        bombObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_BOMB).transform;
        bombObj.name = bomb.name;

        for (int i = 0; i < numOfGoalPosts; i++)
        {
            float posX = 13.25f;
            float posY = 3.95f;
            GameObject goalPostObj = (GameObject)Instantiate(goalPost, goalPost.transform.position, goalPost.transform.rotation);
            goalPostObj.transform.parent = gameObject.transform.GetChild((int)ObjectType.OBJECT_GOALPOST).transform;
            goalPostObj.tag = sc.playersList[i].name;
            goalPostObj.name = sc.playersList[i].name + goalPost.name;

            if (i == 0)
            {
                goalPostObj.transform.position = new Vector3(-posX, posY, goalPostObj.transform.position.z);
            }

            else
            {
                goalPostObj.transform.position = new Vector3(posX, posY, transform.position.z);
                goalPostObj.transform.localScale = new Vector3(-goalPostObj.transform.localScale.x, goalPostObj.transform.localScale.y, goalPostObj.transform.localScale.z);
            }

            // SET ACTIVE BASED ON GAMEMODE
            if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
            {
                bombObj.SetActive(false);
                goalPostObj.SetActive(false);
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
        {
            resetAmmuntions();
            resetCrates();
            resetSnowballs();          
            if (sc.resumeGame)
            {
                spawnCrates();
            }
        }

        resetExplosions();
	}

    public void spawnExplosions(Transform explosive)
    {
        for (int i = 0; i < explosionsList.Count; i++)
        {
            if (!explosionsList[i].activeInHierarchy)
            {
                Vector3 explosivePos = new Vector3(explosive.position.x, explosive.position.y + 0.75f, explosive.position.z);
                explosionsList[i].transform.position = explosivePos;
                explosionsList[i].tag = explosive.tag;

                if (explosionsList[i].tag == "Untagged")
                {
                    BombController bc = explosive.GetComponent<BombController>();
                    explosionsList[i].GetComponent<ExplosionController>().checkExplosionRange(bc.knockBack);
                }
                
                else if (explosionsList[i].tag == "Interactable")
                {
                    ExplodeBarrelTiles barrel = explosive.GetComponent<ExplodeBarrelTiles>();
                    explosionsList[i].GetComponent<ExplosionController>().checkExplosionRange(barrel.knockBack);
                }

                else
                {
                    GrenadeController gc = explosive.GetComponent<GrenadeController>();
                    explosionsList[i].GetComponent<ExplosionController>().checkExplosionRange(gc.knockBack);
                }
                
                explosionsList[i].SetActive(true);
                AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_EXPLOSION, 0.5f);
                break;
            }
        }
    }

    void spawnCrates()
    {
        if (spawnCratesDelay <= 0.0f)
        {
            if (numOfCratesOnScreen < numOfCrates)
            {
                int randomCrateType = Random.Range(0, cratesList.Count);

                // IF RANDOMIZER PICKS THE SAME CRATE, RANDOMIZE AGAIN
                while (cratesIDList.Contains(randomCrateType))
                {
                    randomCrateType = Random.Range(0, cratesList.Count);
                }

                // SPAWN CRATE
                cratesIDList.Add(randomCrateType);
                cratesList[randomCrateType].transform.position = new Vector3(Random.Range(-sc.screenWidth, sc.screenWidth), sc.screenHeight * 1.5f, 0.0f);
                cratesList[randomCrateType].SetActive(true);
                numOfCratesOnScreen++;
            }

            spawnCratesDelay = spawnCratesDelayReset;
        }

        else
        {
            spawnCratesDelay -= Time.deltaTime;
        }
    }

    void resetCrates()
    {
        for (int i = 0; i < cratesList.Count; i++)
        {
            // DESPAWN CRATE
            CrateController crate = cratesList[i].GetComponent<CrateController>();
            if (cratesList[i].activeInHierarchy)
            {
                if (crate.lifetime <= -0.1f)
                {
                    crate.lifetime = crate.lifetimeReset;
                    crate.remove = true;
                }

                else
                {
                    crate.lifetime -= Time.deltaTime;
                }
            }

            // REMOVE CRATE FROM SCREEN
            if (cratesList[i].GetComponent<CrateController>().remove)
            {
                cratesList[i].GetComponent<CrateController>().remove = false;
                cratesList[i].transform.position = cratePistol.transform.position;
                cratesList[i].transform.rotation = cratePistol.transform.rotation;
                cratesList[i].SetActive(false);
                
                for (int x = 0; x < cratesIDList.Count; x++)
                {
                    if (cratesList[i].GetComponent<CrateController>().id == cratesIDList[x])
                    {
                        cratesIDList.RemoveAt(x);
                        numOfCratesOnScreen--;
                    }
                }
            }
        }
    }

    void resetAmmuntions()
    {
        for (int i = 0; i < numOfAmmo; i++)
        {
            if (bulletsList[i].GetComponent<BulletController>().remove)
            {
                bulletsList[i].GetComponent<BulletController>().remove = false;
                bulletsList[i].transform.position = bullet.transform.position;
                bulletsList[i].transform.rotation = bullet.transform.rotation;
                bulletsList[i].transform.localScale = bullet.transform.localScale;
                bulletsList[i].tag = "Untagged";
                bulletsList[i].SetActive(false);
            }

            if (grenadesList[i].GetComponent<GrenadeController>().remove)
            {
                grenadesList[i].GetComponent<GrenadeController>().remove = false;
                grenadesList[i].GetComponent<GrenadeController>().propelledOnce = false;
                grenadesList[i].transform.position = grenade.transform.position;
                grenadesList[i].transform.rotation = grenade.transform.rotation;
                grenadesList[i].tag = "Untagged";
                grenadesList[i].SetActive(false);
            }
        }
    }

    void resetExplosions()
    {
        for (int i = 0; i < explosionsList.Count; i++)
        {
            if (explosionsList[i].GetComponent<ExplosionController>().remove)
            {
                explosionsList[i].GetComponent<ExplosionController>().remove = false;
                explosionsList[i].transform.position = explosion.transform.position;
                explosionsList[i].transform.rotation = explosion.transform.rotation;
                explosionsList[i].SetActive(false);
            }
        }
    }

    void resetSnowballs()
    {
        for (int i = 0; i < snowballsList.Count; i++)
        {
            if (snowballsList[i].GetComponent<SnowBallController>().remove)
            {
                snowballsList[i].GetComponent<SnowBallController>().remove = false;
                snowballsList[i].transform.position = snowball.transform.position;
                snowballsList[i].transform.rotation = snowball.transform.rotation;
                snowballsList[i].SetActive(false);
            }
        }
    }

    public void resetBomb()
    {
        bombObj.GetComponent<BombController>().randomizeExplodeTiming();
        bombObj.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        bombObj.GetComponent<Rigidbody2D>().angularVelocity = 0.0f;
        bombObj.transform.position = bomb.transform.position;
        bombObj.transform.rotation = bomb.transform.rotation;
        bombObj.SetActive(true);
    }

    public void fireBullet(Transform weapon, Transform player, string weaponName, float tragectory)
    {
        for (int i = 0; i < bulletsList.Count; i++)
        {
            if (!bulletsList[i].activeInHierarchy)
            {              
                if (weaponName == "Pistol")
                {
                    bulletsList[i].GetComponent<BulletController>().moveSpeed = 30.0f;
                    bulletsList[i].GetComponent<BulletController>().knockBack = 12.5f;
                }

                else if (weaponName == "Rifle")
                {
                    bulletsList[i].GetComponent<BulletController>().moveSpeed = 40.0f;
                    bulletsList[i].GetComponent<BulletController>().knockBack = 10.0f;
                }
                
                if (player.transform.localScale.x < 0.0f)
                {
                    bulletsList[i].GetComponent<BulletController>().moveSpeed *= -1.0f;
                    bulletsList[i].transform.localScale *= -1.0f;
                }

                bulletsList[i].transform.position = weapon.position;
                bulletsList[i].transform.rotation = Quaternion.Euler(bulletsList[i].transform.rotation.x, bulletsList[i].transform.rotation.y, tragectory);
                bulletsList[i].tag = player.tag;
                bulletsList[i].SetActive(true);          
                break;
            }
        }
    }

    public void fireGrenade(Transform weapon, Transform player)
    {
        for (int i = 0; i < grenadesList.Count; i++)
        {
            if (!grenadesList[i].activeInHierarchy)
            {
                grenadesList[i].GetComponent<GrenadeController>().propelledForce = 7.0f;
                grenadesList[i].GetComponent<GrenadeController>().knockBack = 16.0f;
                
                Vector3 weaponPos = new Vector3(weapon.position.x + 2.0f, weapon.position.y, weapon.position.z);
                if (player.transform.localScale.x < 0.0f)
                {
                    grenadesList[i].GetComponent<GrenadeController>().propelledForce *= -1.0f;
                    weaponPos = new Vector3(weapon.position.x - 2.5f, weapon.position.y, weapon.position.z);
                }

                grenadesList[i].transform.position = weaponPos;
                grenadesList[i].tag = player.tag;
                grenadesList[i].SetActive(true);
                break;
            }
        }
    }

    public void fireSnowball(Vector3 direction, Vector3 startPos)
    {
        for (int i = 0; i < snowballsList.Count; i++)
        {
            if (!snowballsList[i].activeInHierarchy)
            {
                snowballsList[i].GetComponent<SnowBallController>().moveSpeed = 12.5f;
                snowballsList[i].GetComponent<SnowBallController>().knockBack = 12.5f;
                snowballsList[i].GetComponent<SnowBallController>().moveDirection = direction;
                snowballsList[i].GetComponent<SnowBallController>().transform.position = startPos;

                snowballsList[i].SetActive(true);
                break;
            }
        }
    }
}
