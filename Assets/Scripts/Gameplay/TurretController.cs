﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour 
{
    enum turretState
    {
        TURRET_SCOUT,
        TURRET_AIM,
        TURRET_SHOOT,
    };
    turretState state;

    SingletonController sc;
    GameObject aimIndicator;
    GameObject target;
    Color colorChange;

    Vector3 direction;
    float detectionRadius;
    float colorFade;

	// Use this for initialization
	void Start () 
    {
        sc                  = SingletonController.instance;
        aimIndicator        = gameObject.transform.GetChild(0).gameObject;
        colorChange         = aimIndicator.GetComponent<SpriteRenderer>().color;
        state               = turretState.TURRET_SCOUT;
        detectionRadius     = 7.0f;
        colorFade           = 0.01f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (sc.resumeGame)
        {
            switch (state)
            {
                case turretState.TURRET_SCOUT:
                    turretScout();
                    break;

                case turretState.TURRET_AIM:
                    turretAim();
                    break;

                case turretState.TURRET_SHOOT:
                    turretShoot();
                    break;
            }
        }
	}

    void turretScout()
    {
        // ROTATE INDICATOR
        aimIndicator.transform.Rotate(Vector3.forward, 2.0f);

        // CHECK IF PLAYERS WITHIN DETECTION RADIUS
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            float distance = (gameObject.transform.position - sc.playersList[i].transform.position).magnitude;

            // PLAYER DETECTED
            if (distance <= detectionRadius)
            {
                target = sc.playersList[i];
                state = turretState.TURRET_AIM;
            }
        }

        // COLOR FADING TO WHITE
        if (colorChange != Color.white)
        {
            colorChange.g += colorFade;
            colorChange.b += colorFade;
            aimIndicator.GetComponent<SpriteRenderer>().color = colorChange;
        }
    }

    void turretAim()
    {
        float distance = (gameObject.transform.position - target.transform.position).magnitude;

        // PLAYER BROKE DETECTION RADIUS
        if (distance > detectionRadius)
        {
            state = turretState.TURRET_SCOUT;
        }

        // PLAYER WITHIN DETECTION RADIUS
        else
        {
            // AIM INDICATOR TRACES PLAYER
            direction = (target.transform.position - aimIndicator.transform.position).normalized;
            float zAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90.0f;
            aimIndicator.transform.rotation = Quaternion.Euler(0, 0, zAngle);

            // TURRET READY TO SHOOT
            if (colorChange == Color.red)
            {
                state = turretState.TURRET_SHOOT;
            }

            // COLOR FADING TO RED
            else
            {
                colorChange.g -= colorFade;
                colorChange.b -= colorFade;
                aimIndicator.GetComponent<SpriteRenderer>().color = colorChange;
            }
        }
    }

    void turretShoot()
    {
        // FIRE SNOWBALL
        sc.objectSpawner.fireSnowball(direction, gameObject.transform.position);
        
        // SET AIM INDICATOR BACK TO WHITE
        aimIndicator.GetComponent<SpriteRenderer>().color = Color.white;
        colorChange = aimIndicator.GetComponent<SpriteRenderer>().color;

        // SET TURRET BACK TO SCOUT
        state = turretState.TURRET_SCOUT;

        // PLAY SFX
        AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_SNOWBALL, 0.5f);
    }
}
