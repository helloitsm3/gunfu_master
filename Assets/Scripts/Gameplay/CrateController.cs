﻿using UnityEngine;
using System.Collections;

public class CrateController : MonoBehaviour 
{
    int ammoResupply;
    [HideInInspector]
    public int id;
    [HideInInspector]
    public bool remove;
    [HideInInspector]
    public float lifetime;
    [HideInInspector]
    public float lifetimeReset;

	// Use this for initialization
	void Start () 
    {
        ammoResupply    = 10;
        lifetime        = 20.0f;
        lifetimeReset   = lifetime;
	}

    // CRATES GO OUT OF SCREEN
    void OnBecameInvisible()
    {
        //remove = true;
    }
	
	// Update is called once per frame
	void Update () 
    {
	}

    void OnCollisionEnter2D(Collision2D gameobjectCollided)
    {
        if (gameobjectCollided.gameObject.GetComponent<PlayerController>() != null)
        {
            GameObject weapon = gameobjectCollided.gameObject.GetComponent<PlayerController>().weapons;             
            
            if (gameObject.name == "CratePistol")
            {
                weapon.transform.GetChild((int)PlayerController.WeaponList.WEAPON_PISTOL).GetComponent<WeaponController>().ammo += ammoResupply;
            }

            else if (gameObject.name == "CrateRifle")
            {
                weapon.transform.GetChild((int)PlayerController.WeaponList.WEAPON_RIFLE).GetComponent<WeaponController>().ammo += ammoResupply;
            }

            else if (gameObject.name == "CrateLauncher")
            {
                weapon.transform.GetChild((int)PlayerController.WeaponList.WEAPON_LAUNCHER).GetComponent<WeaponController>().ammo += ammoResupply;
            }

            AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_CRATE, 0.1f);
            remove = true;
        }
    }
}
