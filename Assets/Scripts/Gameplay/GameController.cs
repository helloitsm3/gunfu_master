﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour 
{
    public List<GameObject> playerUIList;
    public List<GameObject> playerIndicatorList;
    public List<Text> playerHitCountList;
    public List<Image> playerPoseList;
    public List<Text> playerPoseTextList;
    public Text gameSet;
    public Image gameOverBackground;
    public Image gameBackground;
    public Button backToMainMenu;
    public Text luckyBombText;

    SingletonController sc;

    float offsetX;
    float offsetY;
    float respawnDelay;
    float invulnerableDuration;
    float gameOverDelay;
    float gameOverDelayTimer;
    float scaleMultipler;
    float luckyBombTextDelay;
    float luckyBombTextDelayTimer;
    Vector3 originalScale;
    Vector3 luckyBombOriginalScale;

	// Use this for initialization
	void Start () 
    {
        sc = SingletonController.instance;   

        // SET PLAYER INDICATOR COLORS
        playerIndicatorList[0].GetComponent<Image>().color = Color.red;
        playerIndicatorList[0].transform.GetChild(0).GetComponent<Text>().color = Color.red;
        playerIndicatorList[0].SetActive(true);

        playerIndicatorList[1].GetComponent<Image>().color = Color.blue;
        playerIndicatorList[1].transform.GetChild(0).GetComponent<Text>().color = Color.blue;
        playerIndicatorList[1].SetActive(true);

        // SET HIT COUNT ORIGINAL SCALE SIZE
        originalScale = playerHitCountList[0].transform.localScale;
        
        // SET LUCKY BOMB TEXT
        luckyBombOriginalScale = luckyBombText.transform.localScale;
        luckyBombText.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        // SET GAME BACKGROUND
        if (PlayerPrefs.GetString("GAMEMODE") == "DeathMatch")
        {
            if (PlayerPrefs.GetString("CHECKCUSTOM") == "false")
            {
                gameBackground.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Maps/Backgrounds/" + PlayerPrefs.GetString("Loading_Map_Name"));
            }

            else
            {
                gameBackground.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Maps/Backgrounds/custom");
            }
        }

        else
        {
            gameBackground.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Maps/Backgrounds/" + PlayerPrefs.GetString("GAMEMODE"));
        }

        offsetX                 = 0.05f;
        offsetY                 = 2.1f;
        respawnDelay            = 3.0f;
        invulnerableDuration    = 4.0f;
        gameOverDelay           = 2.0f;
        gameOverDelayTimer      = 0.0f;
        scaleMultipler          = 0.015f;
        luckyBombTextDelay      = 2.5f;
        luckyBombTextDelayTimer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            GameObject player       = sc.playersList[i].gameObject;
            PlayerController pc     = sc.playersList[i].GetComponent<PlayerController>();
            GameObject indicator    = playerIndicatorList[i];
            Text hitCount           = playerHitCountList[i];

            indicatorMovement(player, indicator);
            respawnPlayer(player, pc);
            setPlayerInvulnerable(player, pc);
            displayPlayerHitCount(player, pc, hitCount);
            gameOver(pc);
        }

        if (PlayerPrefs.GetString("GAMEMODE") == "Lucky Bomb")
        {
            spawnBomb();
        }
	}

    void indicatorMovement(GameObject player, GameObject indicator)
    {
        Vector3 playerPos = player.transform.position;
        indicator.transform.position = Vector3.Lerp(indicator.transform.position, new Vector3(playerPos.x - offsetX, playerPos.y + offsetY, playerPos.z), 1.0f);
    }

    void respawnPlayer(GameObject player, PlayerController pc)
    {
        if (pc.respawn)
        {
            if (pc.respawnDelayTimer >= respawnDelay)
            {
                if (PlayerPrefs.GetString("Loading_Map_Name") != "sci - fi")
                {
                    player.transform.position = new Vector3(Random.Range(-sc.screenWidth * 0.5f, sc.screenWidth * 0.5f), sc.screenHeight + 5.0f, 0.0f);
                }

                else
                {
                    for (int i = 0; i < sc.playersList.Count; i++)
                    {
                        if (player.tag == sc.playersList[i].name)
                        {
                            player.GetComponent<PlayerController>().resetOriginalSpawn();
                        }
                    }
                }
                
                player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                pc.respawnDelayTimer = 0.0f;
                pc.respawn = false;
                pc.invulnerable = true;
                pc.respawnSoundOnce = false;
            }

            else
            {
                pc.respawnDelayTimer += Time.deltaTime;
                if (!pc.respawnSoundOnce)
                {
                    AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_FALLING, 0.5f);
                    pc.respawnSoundOnce = true;
                }
            }
        }
    }

    void setPlayerInvulnerable(GameObject player, PlayerController pc)
    {
        if (pc.invulnerable)
        {
            if (pc.invulnerableDurationTimer >= invulnerableDuration)
            {
                pc.GetComponent<SpriteRenderer>().sortingLayerName = "Players";
                pc.invulnerableDurationTimer = 0.0f;
                pc.invulnerable = false;
            }

            // INVULNERABILITY FLICKERING
            else
            {
                pc.invulnerableDurationTimer += Time.deltaTime;
                pc.flicker = !pc.flicker;

                if (pc.flicker)
                {
                    pc.GetComponent<SpriteRenderer>().sortingLayerName = "Invicible";
                }

                else
                {
                    pc.GetComponent<SpriteRenderer>().sortingLayerName = "Players";
                }
            }
        }
    }

    void displayPlayerHitCount(GameObject player, PlayerController pc, Text hitCount)
    {
        if (pc.increaseHitCount)
        {
            Vector3 playerPos = new Vector3(player.transform.position.x, player.transform.position.y + 2.0f, player.transform.position.z);
            hitCount.transform.position = playerPos;
            hitCount.transform.localScale = originalScale;
            hitCount.gameObject.SetActive(false);
            pc.hitCount++;
            pc.shrinkHitCount = false;
            pc.knockBackSoundOnce = false;

            // SET ALPHA
            Color alphaFade = hitCount.color;
            alphaFade.a = 0.0f;
            hitCount.color = alphaFade;

            // SET INCREASE HITCOUNT BACK TO FALSE
            pc.increaseHitCount = false;
        }

        else
        {
            if (pc.knockedBack)
            {
                Vector3 playerHitCountPos   = hitCount.transform.position;
                Vector3 playerHitCountScale = hitCount.transform.localScale;
                float scaleMultiplier       = 0.005f;
                float alphaFadeSpeed        = 0.1f;

                // SCALE AND ALPHA EFFECT
                Color alphaFade = hitCount.color;
                if (!pc.shrinkHitCount)
                {
                    hitCount.transform.localScale = new Vector3(playerHitCountScale.x + scaleMultiplier, playerHitCountScale.y + scaleMultiplier, playerHitCountScale.z + scaleMultiplier);
                    alphaFade.a += alphaFadeSpeed;
                    if (alphaFade.a >= 1.5f)
                    {
                        pc.shrinkHitCount = true;
                    }
                }

                else
                {
                    hitCount.transform.localScale = new Vector3(playerHitCountScale.x - scaleMultiplier, playerHitCountScale.y - scaleMultiplier, playerHitCountScale.z - scaleMultiplier);
                    alphaFade.a -= alphaFadeSpeed;
                }
                
                hitCount.color = alphaFade;
                hitCount.text = pc.hitCount.ToString() + " HITS";
                hitCount.gameObject.SetActive(true);
            }

            else
            {
                // SET ALPHA
                Color alphaFade = hitCount.color;
                alphaFade.a = 0.0f;
                hitCount.color = alphaFade;
            }
        }
    }

    void gameOver(PlayerController pc)
    {
        if (pc.lives == 0)
        {
            pc.pose.GetComponent<PoseController>().win = false;
            gameSet.gameObject.SetActive(true);
            sc.resumeGame = false;

            // GAME SET DISPLAY EFFECT
            if (gameSet.transform.localScale.x < 0.7f)
            {
                Vector3 tempScale = gameSet.transform.localScale;
                gameSet.transform.localScale = new Vector3(tempScale.x + scaleMultipler, tempScale.y + scaleMultipler, tempScale.z + scaleMultipler);
            }

            // SHOW GAME OVER RESULTS
            else
            {
                gameOverDelayTimer += Time.deltaTime;
                if (gameOverDelayTimer >= gameOverDelay)
                {
                    for (int i = 0; i < playerPoseList.Count; i++)
                    {
                        playerPoseList[i].GetComponent<Image>().enabled = true;
                        playerPoseTextList[i].GetComponent<Text>().enabled = true;
                    }

                    gameSet.gameObject.SetActive(false);
                    gameOverBackground.gameObject.SetActive(true);
                    backToMainMenu.gameObject.SetActive(true);
                    
                    // CONTROLLER INPUT TO GO BACK TO MAIN MENU
                    ControllerSingleton.instance.backButton(1, "Main_Menu");
                }
            }
        }
    }

    public void switchToMainMenu()
    {
        SaveLoadManager.LoadMapData().Clear();
        Destroy(ControllerSingleton.instance.gameObject);
        Destroy(AudioSingleton.instance.gameObject);
        SceneManager.LoadScene("Main_Menu");
    }

    void spawnBomb()
    {
        float sizeIncrement = 0.025f;

        if (sc.objectSpawner.bombObj.GetComponent<BombController>().remove)
        {
            AudioSingleton.instance.stopSound(AudioSingleton.SOUNDS.SOUND_TIMEBOMB);
            sc.objectSpawner.bombObj.GetComponent<BombController>().timeBombSoundOnce = false;
            sc.objectSpawner.bombObj.SetActive(false);
            
            // CHECK IF GAME HAS FINISHED
            if (sc.resumeGame)
            {
                // CHECK REMOVE TYPE
                if (sc.objectSpawner.bombObj.GetComponent<BombController>().removeBomb != BombController.REMOVE_BOMB.BOMB_EXPLODE)
                {
                    // CHANGE WHAT LUCKY BOMB TEXT WRITES
                    if (sc.objectSpawner.bombObj.GetComponent<BombController>().removeBomb == BombController.REMOVE_BOMB.BOMB_GOAL)
                    {
                        luckyBombText.GetComponent<Text>().text = "GOAL";
                        if (!sc.objectSpawner.bombObj.GetComponent<BombController>().textSoundOnce)
                        {
                            AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_GOAL, 0.1f);
                            sc.objectSpawner.bombObj.GetComponent<BombController>().textSoundOnce = true;
                        }
                    }

                    else if (sc.objectSpawner.bombObj.GetComponent<BombController>().removeBomb == BombController.REMOVE_BOMB.BOMB_OUT)
                    {
                        luckyBombText.GetComponent<Text>().text = "OUT";
                        if (!sc.objectSpawner.bombObj.GetComponent<BombController>().textSoundOnce)
                        {
                            AudioSingleton.instance.PlaySound(AudioSingleton.SOUNDS.SOUND_OUT, 0.1f);
                            sc.objectSpawner.bombObj.GetComponent<BombController>().textSoundOnce = true;
                        }
                    }

                    // EXPANDING LUCKY BOMB TEXT
                    if (luckyBombText.transform.localScale.x < luckyBombOriginalScale.x)
                    {
                        luckyBombText.gameObject.SetActive(true);
                        Vector3 scale = luckyBombText.transform.localScale;
                        luckyBombText.transform.localScale = new Vector3(scale.x + sizeIncrement, scale.y + sizeIncrement, scale.z);
                    }

                    // LUCKY BOMB TEXT SHOWN
                    else
                    {
                        luckyBombTextDelayTimer += Time.deltaTime;

                        // REMOVE LUCKY BOMB TEXT
                        if (luckyBombTextDelayTimer >= luckyBombTextDelay)
                        {
                            luckyBombTextDelayTimer = 0.0f;
                            luckyBombText.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                            luckyBombText.gameObject.SetActive(false);
                            sc.objectSpawner.resetBomb();
                            sc.objectSpawner.bombObj.GetComponent<BombController>().textSoundOnce = false;
                            sc.objectSpawner.bombObj.GetComponent<BombController>().remove = false;

                            // RESET PLAYER'S POSTION
                            for (int i = 0; i < sc.playersList.Count; i++)
                            {
                                sc.playersList[i].GetComponent<PlayerController>().resetOriginalSpawn();
                            }
                        }
                    }
                }

                else
                {
                    sc.objectSpawner.resetBomb();
                    sc.objectSpawner.bombObj.GetComponent<BombController>().remove = false;
                }
            }
        }
    }
}
