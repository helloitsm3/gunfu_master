﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoseController : MonoBehaviour 
{
    [HideInInspector]
    public bool win;
    
    Animator anim;
    SingletonController sc;

    int characterSelectPlayerNum;
    bool adjustSizeOnce;
    float scaleMultiplier;

	// Use this for initialization
	void Start () 
    {
        win                             = true;
        anim                            = GetComponent<Animator>();
        sc                              = SingletonController.instance;
        adjustSizeOnce                  = false;

        if (gameObject.tag == "Player1")
        {
            characterSelectPlayerNum = 0;
        }

        else if (gameObject.tag == "Player2")
        {
            characterSelectPlayerNum = 1;
        }
	}
	
    void LateUpdate()
    {
        // SET FOR CHARACTERS THAT ARE NOT KINGHT
        if (sc.characterSelect[characterSelectPlayerNum] != sc.characters.Count - 1)
        {
            transform.GetComponent<Image>().SetNativeSize();
        }
    }

	// Update is called once per frame
	void Update () 
    {
        if (transform.GetComponent<Image>().enabled)
        {
            if (win)
            {
                anim.Play("Char" + sc.characterSelect[characterSelectPlayerNum].ToString() + "_Win");
                // SET FOR KNIGHT ONLY
                if (sc.characterSelect[characterSelectPlayerNum] == sc.characters.Count - 1)
                {
                    if (!adjustSizeOnce)
                    {
                        scaleMultiplier = 1.05f;
                        transform.localScale = new Vector3(transform.localScale.x * scaleMultiplier, transform.localScale.y * scaleMultiplier, transform.localScale.z * scaleMultiplier);
                        adjustSizeOnce = true;
                    }
                }
            }

            else
            {
                anim.Play("Char" + sc.characterSelect[characterSelectPlayerNum].ToString() + "_Lose");
                // SET FOR KNIGHT ONLY
                if (sc.characterSelect[characterSelectPlayerNum] == sc.characters.Count - 1)
                {
                    if (!adjustSizeOnce)
                    {
                        scaleMultiplier = 1.3f;
                        transform.localScale = new Vector3(transform.localScale.x * scaleMultiplier, transform.localScale.y * scaleMultiplier, transform.localScale.z * scaleMultiplier);
                        adjustSizeOnce = true;
                    }
                }
            }
        }
	}
}
