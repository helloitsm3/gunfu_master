﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GrenadeController : MonoBehaviour 
{
    [HideInInspector]
    public float propelledForce;
    [HideInInspector]
    public float knockBack;
    [HideInInspector]
    public bool remove;
    [HideInInspector]
    public bool propelledOnce;

    SingletonController sc;
    GameObject player;
    Vector2 pausedVelocity;

	// Use this for initialization
	void Start () 
    {
        remove                      = false;
        propelledOnce               = false;
        sc                          = SingletonController.instance;
        pausedVelocity              = new Vector2(0.0f, 0.0f);

        for (int i = 0; i < sc.playersList.Count; i++)
        {
            if (transform.tag == sc.playersList[i].name)
            {
                player = sc.playersList[i];
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        // LAUNCH GRENADE
        if (gameObject.activeInHierarchy)
        {
            if (!propelledOnce)
            {
                launchGrenade();
                propelledOnce = true;
            }
        }

        // PAUSE GRENADE
        if (!sc.resumeGame)
        {
            if (gameObject.GetComponent<Rigidbody2D>().IsAwake())
            {
                pausedVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
                gameObject.GetComponent<Rigidbody2D>().Sleep();
            }
        }

        else
        {
            if (gameObject.GetComponent<Rigidbody2D>().IsSleeping())
            {
                gameObject.GetComponent<Rigidbody2D>().WakeUp();
                gameObject.GetComponent<Rigidbody2D>().velocity = pausedVelocity;
            }
        }
	}

    // GRENADES GO OUT OF SCREEN
    void OnBecameInvisible()
    {
        remove = true;
    }

    void launchGrenade()
    {
        Rigidbody2D rb = transform.GetComponent<Rigidbody2D>();
        Rigidbody2D playerRB = player.GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        rb.velocity = new Vector2(propelledForce + (playerRB.velocity.x * 0.5f), Mathf.Abs(propelledForce) + (Mathf.Abs(playerRB.velocity.y) * 0.1f));
    }

    void OnCollisionEnter2D(Collision2D gameobjectCollided)
    {
        // CHECK IF OBJECT COLLIDED WITH IS EXPLOSIVE BARREL
        if (gameobjectCollided.gameObject.GetComponent<ExplodeBarrelTiles>())
        {
            ExplodeBarrelTiles barrel = gameobjectCollided.gameObject.GetComponent<ExplodeBarrelTiles>();
            barrel.damageBarrel(barrel.health);
        }
       
        for (int i = 0; i < sc.playersList.Count; i++)
        {
            // CHECK GRENADE BELONGS TO WHICH PLAYER
            if (sc.playersList[i].name == gameObject.tag)
            {
                // CHECK IF CHARACTER IS ROBOT TO USE ULTIMATE
                PlayerController player = sc.playersList[i].GetComponent<PlayerController>();
                if (player.characterUltimate == PlayerController.ultimateList.ULTIMATE_GRENADEBOOST && player.ultimate.activeInHierarchy)
                {
                    // BUG FIX FOR GRENADE OBJECT PULLING
                    if (!remove)
                    {
                        sc.objectSpawner.GetComponent<ObjectSpawner>().spawnExplosions(transform);
                        
                        Transform newTransform = gameObject.transform;
                        float offsetX = 2.0f;
                        if (sc.playersList[i].transform.localScale.x < 0.0f)
                        {
                            offsetX *= -1.0f;
                        }

                        for (int x = 0; x < 2; x++)
                        {
                            newTransform.position = new Vector3(newTransform.position.x + offsetX, newTransform.position.y, newTransform.position.z);
                            sc.objectSpawner.GetComponent<ObjectSpawner>().spawnExplosions(newTransform);
                        }
                        
                        remove = true;
                    }                 
                }

                // NORMAL EXPLOSION
                else
                {
                    // BUG FIX FOR GRENADE OBJECT PULLING
                    if (!remove)
                    {
                        sc.objectSpawner.GetComponent<ObjectSpawner>().spawnExplosions(transform);
                        remove = true;
                    }
                }
            }
        }        
    }
}
