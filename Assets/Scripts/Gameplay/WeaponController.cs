﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour 
{
    [HideInInspector]
    public float fireRate;
    [HideInInspector]
    public float fireRateTimer;
    [HideInInspector]
    public int ammo;

	// Use this for initialization
	void Start () 
    {
        fireRate        = 0.2f;
        fireRateTimer   = fireRate;
	}
	
	// Update is called once per frame
	void Update () 
    {
	}
}
